var _5047ee604ae08900cdea13aa257f5259;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentInspector.ets":
/*!*******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentInspector.ets ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/12/09
  my_annual_flag: 将 getInspectorById 封装起来，便于获取和使用其中的常用属性
 */
class ComponentInspector {
    constructor(id) {
        // 上下左右四个边框坐标，单位 vp
        this.coord_left = 0;
        this.coord_right = 0;
        this.coord_top = 0;
        this.coord_bottom = 0;
        //尺寸
        this._width = 0;
        this._height = 0;
        const attrsStr = getInspectorByKey(id);
        if (!attrsStr) {
            console.error(`ComponentInspector: id: ${id} not found`);
        }
        this.attrs = JSON.parse(attrsStr);
        let rectInfo = JSON.parse('[' + this.attrs.$rect + ']');
        this.coord_left = px2vp(parseInt(JSON.parse('[' + rectInfo[0] + ']')[0], 10));
        this.coord_top = px2vp(parseInt(JSON.parse('[' + rectInfo[0] + ']')[1], 10));
        this.coord_right = px2vp(parseInt(JSON.parse('[' + rectInfo[1] + ']')[0], 10));
        this.coord_bottom = px2vp(parseInt(JSON.parse('[' + rectInfo[1] + ']')[1], 10));
        this._width = this.coord_right - this.coord_left;
        this._height = this.coord_bottom - this.coord_top;
    }
    getWidth() {
        return this._width;
    }
    getHeight() {
        return this._height;
    }
    toString() {
        return JSON.parse(this.attrs);
    }
    getBottom() {
        return this.coord_bottom;
    }
}
exports["default"] = ComponentInspector;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getTheme = void 0;
const ThemeConst_1 = __webpack_require__(/*! ../model/ThemeConst */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets");
// 根据全局变量theme来控制模式
function getTheme(theme) {
    return ThemeConst_1.DefaultTheme;
}
exports.getTheme = getTheme;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/9/24
  my_annual_flag: 云侧接口调用
 */
var _ohos_net_http_1  = globalThis.requireNapi('net.http');
const mockData_1 = __webpack_require__(/*! ./mockData */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets");
const Goals_1 = __webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const isMock = true;
const mockDelay = 1000;
const achievementDomain = 'https://lfhealthtest2.hwcloudtest.cn:18444/achievement';
//const huid = '4130086000005055643';
// 13019409302
const huid = '10086000011925744';
const commonBody = {
    appId: 'com.huawei.health',
    appType: 1,
    deviceId: '4fe5240a56f37a11',
    deviceType: 9,
    // token: '0413008600000505564356624a402cc253ef201f90c7230bd25b5060d2e466b1e85fa939d999cd3e7152',
    // 13019409302
    token: '00010086000011925744fdf226b8bfbfefa73011ecca54a0819cd50d0d18ab008273c6e8aa7c86342cda',
    tokenType: 1,
    ts: new Date().getTime(),
};
class CloudApis {
    //  options: {extraData: {}}
    constructor() {
        this.domain = achievementDomain;
        this.options = {
            method: _ohos_net_http_1.RequestMethod.POST,
            extraData: commonBody,
            header: {
                'Content-Type': 'application/json',
                'x-huid': huid
            },
            readTimeout: 10000,
            connectTimeout: 10000
        };
    }
    // 获取 6 种目标的统计数据（如现激活人数等）
    async getGoalStatistics() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalStatistics.result);
            //}, mockDelay))
            resResultObj = mockData_1.mockCloudData.getGoalStatistics.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalStatistics`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const todoGoals = resResultObj.statistics.map((e) => {
            return new Goals_1.TodoGoal(e.goalType, e.activeNum);
        });
        return todoGoals;
    }
    // 获取本用户激活的目标的各种数据（如已完成数值、目标数值等）
    async getGoalList() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalList.result);
            //}, mockDelay));
            resResultObj = mockData_1.mockCloudData.getGoalList.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalList}`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const addedGoals = resResultObj.goal.map((e) => {
            return new Goals_1.AddedGoal(e.id, e.goalType, e.completeValue, e.goalValue, e.currentWeight, e.recordDays);
        });
        return addedGoals;
    }
    // 添加新目标（除了体重目标）
    async addGoal(goalType, goalValue) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: goalType, goalValue: goalValue, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 添加体重目标
    async addWeightGoal(initWeight, targetWeight) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: Goals_1.GoalType.Weight, goalValue: targetWeight, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addWightGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 删除目标
    async delGoal(addedGoal) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.delGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ records: [addedGoal.id] }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/delGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: delGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 修改编辑目标
    async updateGoal(addedGoal, goalValue, currentWeight = 0) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.updateGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, id: addedGoal.id, goalValue: goalValue, frequency: 0, unitValue: 0, currentWeight: 0 }, commonBody);
            if (addedGoal.goalType === Goals_1.GoalType.Weight) {
                body.currentWeight = currentWeight;
            }
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/updateGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: updateGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
}
exports["default"] = new CloudApis();


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets":
/*!*******************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const consts = {
    // TODO: 硬编码状态栏高度，单位 vp。（未知，估计的，待新版本提供接口）
    STATUS_BAR_HEIGHT: 24,
    // TODO: 硬编码导航栏高度，单位 vp。（通过 getInspectorByKey 方式间接获取的，暂不知道如何直接获取导航栏高度，故硬编码）
    NAV_BAR_HEIGHT: 40,
};
exports["default"] = consts;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets":
/*!********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getDefaultDisplaySizeInVP = exports.setNextPageToastMessage = exports.showToastIfNeeded = exports.getTimeRange = exports.numberWithCommas = exports.maxDecimals = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/13
  my_annual_flag: 工具函数
 */
var _ohos_prompt_1  = globalThis.requireNapi('prompt');
var _ohos_display_1  = globalThis.requireNapi('display');
/**
 * 让数字最多保留 x 位小鼠
 * @param num 需要简化的数字
 * @param maxDecimals 最多保留的小数位数
 */
function maxDecimals(num, maxDecimals) {
    return Math.round(num * Math.pow(10, maxDecimals)) / Math.pow(10, maxDecimals);
}
exports.maxDecimals = maxDecimals;
/**
 * 给数字每千位添加一个逗号
 * @param x 数字
 * @return 添加过逗号的字符串
 */
function numberWithCommas(x) {
    return maxDecimals(x, 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
exports.numberWithCommas = numberWithCommas;
/**
 * 返回调用时当年的第一天和最后一天
 * @return 2.g. '2022/01/01-2022/12/31'
 */
function getTimeRange() {
    const date = new Date();
    const currentYear = date.getFullYear();
    const startDate = `${currentYear}/01/01`;
    const endDate = `${currentYear}12/31`;
    return `${startDate}-${endDate}`;
}
exports.getTimeRange = getTimeRange;
// 放在 onPageShow, 在页面启动时，根据是否设置 toast 字串来决定是否展示 toast
function showToastIfNeeded() {
    const msg = AppStorage.Get('toastMessage');
    if (msg) {
        _ohos_prompt_1.showToast({ message: msg });
    }
    AppStorage.SetOrCreate('toastMessage', '');
}
exports.showToastIfNeeded = showToastIfNeeded;
/**
 * 设置 toast 信息，并在下次切换页面时显示
 * @param msg 需要显示的文字
 */
function setNextPageToastMessage(msg) {
    AppStorage.SetOrCreate('toastMessage', msg);
}
exports.setNextPageToastMessage = setNextPageToastMessage;
/**
 * 获得默认显示屏的屏幕大小，vp 为单位
 * @return { width: xx, height: xx }
 */
function getDefaultDisplaySizeInVP() {
    console.log('start getDefaultDisplaySync');
    const defaultDisplay = _ohos_display_1.getDefaultDisplaySync();
    console.log('getDefaultDisplaySync' + defaultDisplay);
    return {
        width: px2vp(defaultDisplay.width),
        height: px2vp(defaultDisplay.height),
    };
    //  return {
    //    width: 360,
    //    height: 740,
    //  };
}
exports.getDefaultDisplaySizeInVP = getDefaultDisplaySizeInVP;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.signedDates = exports.mockCloudData = void 0;
exports.mockCloudData = {
    // 获得所有参与者的目标统计数据（坚持人数，完成人数 等...）
    getGoalStatistics: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "statistics": [
                {
                    "goalType": 0,
                    "createNum": 3801,
                    "activeNum": 1367,
                    "completeNum": 1243,
                    "delNum": 906,
                    "timeoutNum": 285
                },
                {
                    "goalType": 1,
                    "createNum": 3309,
                    "activeNum": 897,
                    "completeNum": 1241,
                    "delNum": 886,
                    "timeoutNum": 285
                },
                {
                    "goalType": 2,
                    "createNum": 1436,
                    "activeNum": 838,
                    "completeNum": 14,
                    "delNum": 143,
                    "timeoutNum": 441
                },
                {
                    "goalType": 3,
                    "createNum": 916,
                    "activeNum": 847,
                    "completeNum": 0,
                    "delNum": 67,
                    "timeoutNum": 2
                },
                {
                    "goalType": 4,
                    "createNum": 1964,
                    "activeNum": 474,
                    "completeNum": 1249,
                    "delNum": 240,
                    "timeoutNum": 1
                },
                {
                    "goalType": 5,
                    "createNum": 906,
                    "activeNum": 833,
                    "completeNum": 0,
                    "delNum": 72,
                    "timeoutNum": 1
                }
            ]
        }
    },
    getGoalList: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "goal": [
                {
                    "completeValue": 0,
                    "createTime": 1658217778708,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 1,
                    "goalValue": 1234,
                    "id": 23557,
                    "modifyTime": 1663845225603,
                    "recordDays": [],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 120.197,
                    "createTime": 1658217813686,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 3,
                    "goalValue": 1234,
                    "id": 23559,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 55.4,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 4,
                    "goalValue": 52.1,
                    "id": 23560,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 1,
                    "unitValue": 0
                },
                {
                    "completeValue": 1153834,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 2,
                    "goalValue": 1234567,
                    "id": 23561,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                }
            ]
        }
    },
    addGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    delGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    updateGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    }
};
/**
 * 生成数个今年的随机日期
 * @param numOfDays 需要随机生成日期的数量
 * 返回随机日期数组（unix 时间戳）
 */
const generateRandomDates = function (numOfDays) {
    const currentYear = new Date().getFullYear();
    const firstDayOfYear = new Date(currentYear, 0, 1);
    const lastDayOfYear = new Date(currentYear, 11, 31);
    const res = new Set();
    for (let i = 0; i < numOfDays; i++) {
        let unique = false;
        while (!unique) {
            const randomDate = new Date(firstDayOfYear.getTime() + Math.random() * (lastDayOfYear.getTime() - firstDayOfYear.getTime())).toDateString();
            if (!res.has(randomDate)) {
                res.add(randomDate);
                unique = true;
            }
        }
    }
    return res;
};
exports.signedDates = generateRandomDates(240);


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets":
/*!*******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets ***!
  \*******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProgressCircle = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/17
  my_annual_flag: 应用在主页和日历页的进度环
 */
const Color_1 = __importDefault(__webpack_require__(/*! ../model/Color */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets"));
//TODO:已知模拟器默认canvas大小360*360，目前根据这个写死各种缩放逻辑，后续优化
const DEFAULT_CANVAS_SIZE = 360;
class ProgressCircle extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.slot = undefined;
        this.lineWidth = 50;
        this.bgStartAngle = 0;
        this.bgEndAngle = 2 * Math.PI;
        this.arcStartAngle = 0;
        this.arcEndAngle = 1 * Math.PI;
        this.startColor = new Color_1.default(251, 215, 134);
        this.endColor = new Color_1.default(247, 121, 125);
        this.isCounterClockWise = false;
        this.canvasSize = DEFAULT_CANVAS_SIZE;
        this.settings = new RenderingContextSettings(true);
        this.ctx = new CanvasRenderingContext2D(this.settings);
        this.drawn = false;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.slot !== undefined) {
            this.slot = params.slot;
        }
        if (params.lineWidth !== undefined) {
            this.lineWidth = params.lineWidth;
        }
        if (params.bgStartAngle !== undefined) {
            this.bgStartAngle = params.bgStartAngle;
        }
        if (params.bgEndAngle !== undefined) {
            this.bgEndAngle = params.bgEndAngle;
        }
        if (params.arcStartAngle !== undefined) {
            this.arcStartAngle = params.arcStartAngle;
        }
        if (params.arcEndAngle !== undefined) {
            this.arcEndAngle = params.arcEndAngle;
        }
        if (params.startColor !== undefined) {
            this.startColor = params.startColor;
        }
        if (params.endColor !== undefined) {
            this.endColor = params.endColor;
        }
        if (params.isCounterClockWise !== undefined) {
            this.isCounterClockWise = params.isCounterClockWise;
        }
        if (params.canvasSize !== undefined) {
            this.canvasSize = params.canvasSize;
        }
        if (params.settings !== undefined) {
            this.settings = params.settings;
        }
        if (params.ctx !== undefined) {
            this.ctx = params.ctx;
        }
        if (params.drawn !== undefined) {
            this.drawn = params.drawn;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    /**
     * 计算渐变颜色
     * @param start 初始颜色
     * @param end 结束颜色
     * @param step 渐变段数（段数越高，渐变越平滑）
     * @return 颜色数组（长度等于step）
     */
    gradientColor(start, end, step) {
        const arr = [];
        const diffR = (end.r - start.r) / step;
        const diffG = (end.g - start.g) / step;
        const diffB = (end.b - start.b) / step;
        for (let i = 0; i < step; i++) {
            const color = new Color_1.default(diffR * i + start.r, diffG * i + start.g, diffB * i + start.b);
            arr.push(color);
        }
        return arr;
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.Center });
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Canvas.create(this.ctx);
            Canvas.size({ width: this.canvasSize, height: this.canvasSize });
            Canvas.onReady(() => {
                if (this.drawn)
                    return;
                //圆环背景
                this.ctx.beginPath();
                const s = this.canvasSize / DEFAULT_CANVAS_SIZE;
                this.ctx.translate(DEFAULT_CANVAS_SIZE / 2 * s, DEFAULT_CANVAS_SIZE / 2 * s);
                this.ctx.scale(s, s);
                this.ctx.lineWidth = this.lineWidth;
                this.ctx.strokeStyle = 'rgba(0,0,0,0.03)';
                this.ctx.lineCap = 'round';
                const radius = DEFAULT_CANVAS_SIZE / 2 - this.lineWidth / 2;
                this.ctx.arc(0, 0, radius, this.bgStartAngle, this.bgEndAngle, this.isCounterClockWise);
                this.ctx.stroke();
                this.ctx.closePath();
                const unit = 0.05;
                const progress = (this.arcStartAngle - this.arcStartAngle)
                    / (this.bgEndAngle - this.bgStartAngle);
                if (progress >= 0.01 && progress < 0.02) {
                    this.ctx.beginPath();
                    this.ctx.arc(0, 0, radius, Math.PI * 3 / 2, Math.PI * 3 / 2, false);
                    this.ctx.strokeStyle = this.startColor.toHex();
                    this.ctx.lineCap = 'round';
                    this.ctx.stroke();
                    this.ctx.closePath();
                    return;
                }
                const division = Math.floor((this.arcEndAngle - this.arcStartAngle) / unit);
                const gradient = this.gradientColor(this.startColor, this.endColor, division);
                let start = this.arcStartAngle;
                let end = start;
                for (let i = 0; i < division; i++) {
                    this.ctx.beginPath();
                    this.ctx.lineCap = 'round';
                    end = start + unit;
                    this.ctx.lineWidth = this.lineWidth;
                    this.ctx.strokeStyle = gradient[i].toRGB();
                    this.ctx.arc(0, 0, radius, start, end);
                    this.ctx.stroke();
                    start += unit;
                }
                this.drawn = true;
            });
            if (!isInitialRender) {
                Canvas.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Canvas.pop();
        this.slot.bind(this)();
        Stack.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
exports.ProgressCircle = ProgressCircle;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/SignCalendar.ets":
/*!*****************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/SignCalendar.ets ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SignCalendar = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/24
  my_annual_flag: 签到日历组件，用于我的目标详情页下方上拉 panel 中。
 */
__webpack_require__(/*! ../common/mockData */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets");
__webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
class SignCalendar extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.currentYear = new Date().getFullYear();
        this.recordDaysTimeStamps = [];
        this.recordDays = undefined;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.currentYear !== undefined) {
            this.currentYear = params.currentYear;
        }
        if (params.recordDaysTimeStamps !== undefined) {
            this.recordDaysTimeStamps = params.recordDaysTimeStamps;
        }
        if (params.recordDays !== undefined) {
            this.recordDays = params.recordDays;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    aboutToAppear() {
        this.recordDays = new Set();
        this.recordDaysTimeStamps.forEach((timestamp) => {
            this.recordDays.add((new Date(timestamp)).toDateString());
        });
    }
    // 日历中的圆点
    CalendarCircle(signed, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Circle.create({ width: 10, height: 10 });
            Circle.fill(signed ? 0xff841a : 0xd8d8d8);
            Circle.opacity(0.6);
            Circle.margin({ top: 4, bottom: 4 });
            if (!isInitialRender) {
                Circle.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
    }
    /**
     * 获得今年某一月第一天是星期几
     * @param monthIndex 月份索引（0 到 11）
     * @return 返回星期几（0 到 6，分别是星期日、星期一 ...）
     */
    getFirstWeekDayOfMonth(monthIndex) {
        return new Date(this.currentYear, monthIndex, 1).getDay();
    }
    /**
     * 获得今年某一月天数
     * @param monthIndex 月份索引（0 到 11）
     * @return 该月天数
     */
    getNumOfDays(monthIndex) {
        return new Date(this.currentYear, monthIndex + 1, 0).getDate();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.height(25);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = () => {
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Row.create();
                    Row.width('50%');
                    if (!isInitialRender) {
                        Row.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Column.create();
                    Column.width('17%');
                    if (!isInitialRender) {
                        Column.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Column.pop();
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Column.create();
                    Column.width('83%');
                    if (!isInitialRender) {
                        Column.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Grid.create();
                    Grid.columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr');
                    if (!isInitialRender) {
                        Grid.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('日');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('日');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('一');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('一');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('二');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('二');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('三');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('三');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('四');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('四');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('五');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('五');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                {
                    const isLazyCreate =  true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('六');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('六');
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
                Grid.pop();
                Column.pop();
                Row.pop();
            };
            this.forEachUpdateFunction(elmtId, Array(2).fill(0), forEachItemGenFunction);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.height(300);
            Scroll.scrollBar(BarState.Off);
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({
                direction: FlexDirection.Row,
                wrap: FlexWrap.Wrap,
            });
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = (_item, m) => {
                const _ = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Row.create();
                    Row.alignItems(VerticalAlign.Top);
                    Row.width('50%');
                    Row.height(120);
                    if (!isInitialRender) {
                        Row.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Column.create();
                    Column.alignItems(HorizontalAlign.End);
                    Column.width('17%');
                    if (!isInitialRender) {
                        Column.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Text.create(`${12 - m}月`);
                    Text.margin({ top: 2, right: -2 });
                    if (!isInitialRender) {
                        Text.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Text.pop();
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Row.create();
                    Row.width(18);
                    Row.height(2);
                    Row.borderRadius(4);
                    Row.margin({ top: 2 });
                    Row.linearGradient({
                        direction: GradientDirection.Right,
                        colors: [['rgba(251, 101, 34, 0.5)', 0.0], ['rgba(255, 149, 71, 0.3)', 1.0]]
                    });
                    if (!isInitialRender) {
                        Row.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Row.pop();
                Column.pop();
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Column.create();
                    Column.width('83%');
                    if (!isInitialRender) {
                        Column.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Grid.create();
                    Grid.hitTestBehavior(HitTestMode.None);
                    Grid.columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr');
                    if (!isInitialRender) {
                        Grid.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    ForEach.create();
                    const forEachItemGenFunction = () => {
                        {
                            const isLazyCreate =  true && (Grid.willUseProxy() === true);
                            const itemCreation = (elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                GridItem.create(deepRenderFunction, isLazyCreate);
                                if (!isInitialRender) {
                                    GridItem.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            };
                            const observedShallowRender = () => {
                                this.observeComponentCreation(itemCreation);
                                GridItem.pop();
                            };
                            const observedDeepRender = () => {
                                this.observeComponentCreation(itemCreation);
                                GridItem.pop();
                            };
                            const deepRenderFunction = (elmtId, isInitialRender) => {
                                itemCreation(elmtId, isInitialRender);
                                this.updateFuncByElmtId.set(elmtId, itemCreation);
                                GridItem.pop();
                            };
                            if (isLazyCreate) {
                                observedShallowRender();
                            }
                            else {
                                observedDeepRender();
                            }
                        }
                    };
                    this.forEachUpdateFunction(elmtId, Array(this.getFirstWeekDayOfMonth(11 - m)).fill(0), forEachItemGenFunction, (_, i) => `vacuum_${12 - m}-${i}`, false, true);
                    if (!isInitialRender) {
                        ForEach.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                ForEach.pop();
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    ForEach.create();
                    const forEachItemGenFunction = (_item, d) => {
                        const _ = _item;
                        {
                            const isLazyCreate =  true && (Grid.willUseProxy() === true);
                            const itemCreation = (elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                GridItem.create(deepRenderFunction, isLazyCreate);
                                if (!isInitialRender) {
                                    GridItem.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            };
                            const observedShallowRender = () => {
                                this.observeComponentCreation(itemCreation);
                                GridItem.pop();
                            };
                            const observedDeepRender = () => {
                                this.observeComponentCreation(itemCreation);
                                this.CalendarCircle.bind(this)(this.recordDays.has((new Date(this.currentYear, 11 - m, d + 1)).toDateString()));
                                GridItem.pop();
                            };
                            const deepRenderFunction = (elmtId, isInitialRender) => {
                                itemCreation(elmtId, isInitialRender);
                                this.updateFuncByElmtId.set(elmtId, itemCreation);
                                this.CalendarCircle.bind(this)(this.recordDays.has((new Date(this.currentYear, 11 - m, d + 1)).toDateString()));
                                GridItem.pop();
                            };
                            if (isLazyCreate) {
                                observedShallowRender();
                            }
                            else {
                                observedDeepRender();
                            }
                        }
                    };
                    this.forEachUpdateFunction(elmtId, Array(this.getNumOfDays(11 - m)).fill(0), forEachItemGenFunction, (_, d) => `date_${12 - m}-${d + 1}`, true, true);
                    if (!isInitialRender) {
                        ForEach.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                ForEach.pop();
                Grid.pop();
                Column.pop();
                Row.pop();
            };
            this.forEachUpdateFunction(elmtId, Array(12).fill(0), forEachItemGenFunction, (_, m) => `month_${12 - m}`, true, true);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // TODO: 硬编码一行日历的高度撑起滚动条，解决滑不到底的问题
            Column.create();
            // TODO: 硬编码一行日历的高度撑起滚动条，解决滑不到底的问题
            Column.height(300);
            if (!isInitialRender) {
                // TODO: 硬编码一行日历的高度撑起滚动条，解决滑不到底的问题
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // TODO: 硬编码一行日历的高度撑起滚动条，解决滑不到底的问题
        Column.pop();
        Flex.pop();
        Scroll.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
exports.SignCalendar = SignCalendar;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/17
  my_annual_flag: 颜色对象
 */
class Color {
    constructor(r, g, b) {
        this.r = 0;
        this.g = 0;
        this.b = 0;
        this.r = Math.round(r);
        this.g = Math.round(g);
        this.b = Math.round(b);
    }
    toHex() {
        return `#${this.num2Hex(this.r)}${this.num2Hex(this.g)}${this.num2Hex(this.b)}`;
    }
    toRGB() {
        return `rgb(${this.r}, ${this.g}, ${this.b})`;
    }
    num2Hex(n) {
        const hex = n.toString(16);
        return hex.length == 1 ? `0${hex}}` : hex;
    }
}
exports["default"] = Color;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AddedGoal = exports.TodoGoal = exports.GoalType = void 0;
const Color_1 = __importDefault(__webpack_require__(/*! ./Color */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets"));
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
var GoalType;
(function (GoalType) {
    GoalType[GoalType["Running"] = 0] = "Running";
    GoalType[GoalType["Fitness"] = 1] = "Fitness";
    GoalType[GoalType["Walking"] = 2] = "Walking";
    GoalType[GoalType["Cycling"] = 3] = "Cycling";
    GoalType[GoalType["Weight"] = 4] = "Weight";
    GoalType[GoalType["Swimming"] = 5] = "Swimming";
})(GoalType = exports.GoalType || (exports.GoalType = {}));
const goalsResourceMap = [
    {
        name: { "id": 16777224, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777291, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777255, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777246, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777283, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起奔跑，最美的风景是下一公里！`,
        unit: '公里'
    },
    {
        name: { "id": 16777223, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777288, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777252, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777245, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777282, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，享受流汗的快乐！`,
        unit: '分钟'
    },
    {
        name: { "id": 16777226, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(255, 163, 51), new Color_1.default(255, 133, 25)],
        imgSrc: { "id": 16777276, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777253, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777244, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777286, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起向前，走向广阔世界！`,
        unit: '步'
    },
    {
        name: { "id": 16777222, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777292, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777254, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777247, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777280, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起骑行，战胜风，也享受风！`,
        unit: '公里'
    },
    {
        name: { "id": 16777227, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777293, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777274, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777248, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777281, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，将自律进行到底！`,
        unit: '公斤'
    },
    {
        name: { "id": 16777225, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777295, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777256, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777249, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777284, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，水的阻力，不及你的毅力！`,
        unit: '公里'
    }
];
class TodoGoal {
    constructor(goalType, activeNum) {
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.image = goalsResourceMap[this.goalType].imgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.addImage = goalsResourceMap[this.goalType].addImgSrc;
        this.activeNum = activeNum;
        this.slogan = goalsResourceMap[this.goalType].slogan(helpers_1.numberWithCommas(activeNum));
    }
}
exports.TodoGoal = TodoGoal;
// 已被用户添加并且跟踪目标
class AddedGoal {
    constructor(id, goalType, completeValue, goalValue, initWeight, recordDaysTimeStamps) {
        this.id = id;
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.icon = goalsResourceMap[this.goalType].iconSrc;
        this.shareImg = goalsResourceMap[this.goalType].shareImgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.completeValue = completeValue;
        this.goalValue = goalValue;
        this.color = goalsResourceMap[this.goalType].color;
        this.initWeight = initWeight;
        this.isWeight = this.goalType === 4;
        this.recordDaysTimeStamps = recordDaysTimeStamps;
        this.progress = this.isWeight ? this.completeValue / this.goalValue : this.completeValue / this.goalValue;
        this.progress = this.isWeight
            ? computeWeightProgress(this.initWeight, this.completeValue, this.goalValue)
            : computeProgress(0, this.completeValue, this.goalValue);
    }
}
exports.AddedGoal = AddedGoal;
// 计算目标完成进度
function computeProgress(start, curr, target) {
    const res = (curr - start) / (target - start);
    if (res < 0)
        return 0;
    if (res >= 0.99 && res < 0.9995)
        return 0.99;
    if (res >= 0.9995)
        return 1;
    return res;
}
// 计算体重目标完成进度
function computeWeightProgress(start, curr, target) {
    if (curr === 0) {
        return 0;
    }
    if (target > start) {
        if (curr <= start) {
            return 0;
        }
        if (curr >= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else if (target < start) {
        if (curr <= start) {
            return 0;
        }
        if (curr <= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else {
        return 1;
    }
}


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DefaultTheme = void 0;
//所有能配置主题的相关资源
//// BACKGROUND_CARD 卡片背景颜色
//// BACKGROUND_MAIN 主页背景色
//// FONT_COLOR_MAIN 字体背景颜色
////CARD_PANEL_BACKGROUND panel弹窗背景颜色（日历，设定目标）
//// START_WINDOW_BACKGROUND ??
//// IC_BACK 返回图标
//// IC_PUBLIC_DEL 删除图标
//// IC_PUBLIC_DET 告警（详情）图标
//// IC_PUBLIC_EDIT 修改图标
//// IC_PUBLIC_HIS 历史图标
//// IC_PUBLIC_MORE 修改删除图标
//// IC_PUBLIC_SHARE 分享图标
//// 系统主题 包括 深浅模式 通过dark，light目录实现
class DefaultTheme {
}
exports.DefaultTheme = DefaultTheme;
DefaultTheme.BACKGROUND_CARD = { "id": 16777238, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BACKGROUND_MAIN = { "id": 16777239, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.FONT_COLOR_MAIN = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.START_WINDOW_BACKGROUND = { "id": 16777241, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.CARD_PANEL_BACKGROUND = { "id": 16777237, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BIG_BTN_BACKGROUND_COLOR = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_BACK = { "id": 16777257, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DEL = { "id": 16777262, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DET = { "id": 16777264, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_EDIT = { "id": 16777266, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_HIS = { "id": 16777268, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_MORE = { "id": 16777270, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_SHARE = { "id": 16777299, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/flag-calendar.ets?entry":
/*!*******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/flag-calendar.ets?entry ***!
  \*******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Create: 2022/10/14
  my_annual_flag: 目标日历页
*/
var _ohos_router_1  = globalThis.requireNapi('router');
const ProgressCircle_1 = __webpack_require__(/*! ../components/ProgressCircle */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets");
const SignCalendar_1 = __webpack_require__(/*! ../components/SignCalendar */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/SignCalendar.ets");
const Goals_1 = __webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
const cloudApis_1 = __importDefault(__webpack_require__(/*! ../common/cloudApis */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets"));
__webpack_require__(/*! ../common/consts */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets");
const ComponentInspector_1 = __importDefault(__webpack_require__(/*! ../common/ComponentInspector */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentInspector.ets"));
const ThemeGet_1 = __webpack_require__(/*! ../common/ThemeGet */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets");
class FlagCalendar extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__goal = new ObservedPropertyObjectPU(new Goals_1.AddedGoal(0, 0, 0, 0, 0, []), this, "goal");
        this.__goalType = new ObservedPropertySimplePU(_ohos_router_1.getParams()['goalType'], this, "goalType");
        this.__activeNum = new ObservedPropertySimplePU(_ohos_router_1.getParams()['activeNum'], this, "activeNum");
        this.__isLoading = new ObservedPropertySimplePU(true, this, "isLoading");
        this.__numOfSignedDays = new ObservedPropertySimplePU(0, this, "numOfSignedDays");
        this.__numOfContinuousSignedDays = new ObservedPropertySimplePU(0, this, "numOfContinuousSignedDays");
        this.__panelMaskColor = new ObservedPropertySimplePU('', this, "panelMaskColor");
        this.__panelHalfHeight = new ObservedPropertySimplePU(225, this, "panelHalfHeight");
        this.__isPanelShow = new ObservedPropertySimplePU(false, this, "isPanelShow");
        this.__stateBarHeight = this.createStorageLink('stateBarHeight', 0, "stateBarHeight");
        this.__navigationBarHeight = this.createStorageLink('navigationBarHeight', 0, "navigationBarHeight");
        this.__theme = this.createStorageLink('theme', -1, "theme");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.goal !== undefined) {
            this.goal = params.goal;
        }
        if (params.goalType !== undefined) {
            this.goalType = params.goalType;
        }
        if (params.activeNum !== undefined) {
            this.activeNum = params.activeNum;
        }
        if (params.isLoading !== undefined) {
            this.isLoading = params.isLoading;
        }
        if (params.numOfSignedDays !== undefined) {
            this.numOfSignedDays = params.numOfSignedDays;
        }
        if (params.numOfContinuousSignedDays !== undefined) {
            this.numOfContinuousSignedDays = params.numOfContinuousSignedDays;
        }
        if (params.panelMaskColor !== undefined) {
            this.panelMaskColor = params.panelMaskColor;
        }
        if (params.panelHalfHeight !== undefined) {
            this.panelHalfHeight = params.panelHalfHeight;
        }
        if (params.isPanelShow !== undefined) {
            this.isPanelShow = params.isPanelShow;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__goal.purgeDependencyOnElmtId(rmElmtId);
        this.__goalType.purgeDependencyOnElmtId(rmElmtId);
        this.__activeNum.purgeDependencyOnElmtId(rmElmtId);
        this.__isLoading.purgeDependencyOnElmtId(rmElmtId);
        this.__numOfSignedDays.purgeDependencyOnElmtId(rmElmtId);
        this.__numOfContinuousSignedDays.purgeDependencyOnElmtId(rmElmtId);
        this.__panelMaskColor.purgeDependencyOnElmtId(rmElmtId);
        this.__panelHalfHeight.purgeDependencyOnElmtId(rmElmtId);
        this.__isPanelShow.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__goal.aboutToBeDeleted();
        this.__goalType.aboutToBeDeleted();
        this.__activeNum.aboutToBeDeleted();
        this.__isLoading.aboutToBeDeleted();
        this.__numOfSignedDays.aboutToBeDeleted();
        this.__numOfContinuousSignedDays.aboutToBeDeleted();
        this.__panelMaskColor.aboutToBeDeleted();
        this.__panelHalfHeight.aboutToBeDeleted();
        this.__isPanelShow.aboutToBeDeleted();
        this.__stateBarHeight.aboutToBeDeleted();
        this.__navigationBarHeight.aboutToBeDeleted();
        this.__theme.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get goal() {
        return this.__goal.get();
    }
    set goal(newValue) {
        this.__goal.set(newValue);
    }
    get goalType() {
        return this.__goalType.get();
    }
    set goalType(newValue) {
        this.__goalType.set(newValue);
    }
    get activeNum() {
        return this.__activeNum.get();
    }
    set activeNum(newValue) {
        this.__activeNum.set(newValue);
    }
    get isLoading() {
        return this.__isLoading.get();
    }
    set isLoading(newValue) {
        this.__isLoading.set(newValue);
    }
    get numOfSignedDays() {
        return this.__numOfSignedDays.get();
    }
    set numOfSignedDays(newValue) {
        this.__numOfSignedDays.set(newValue);
    }
    get numOfContinuousSignedDays() {
        return this.__numOfContinuousSignedDays.get();
    }
    set numOfContinuousSignedDays(newValue) {
        this.__numOfContinuousSignedDays.set(newValue);
    }
    get panelMaskColor() {
        return this.__panelMaskColor.get();
    }
    set panelMaskColor(newValue) {
        this.__panelMaskColor.set(newValue);
    }
    get panelHalfHeight() {
        return this.__panelHalfHeight.get();
    }
    set panelHalfHeight(newValue) {
        this.__panelHalfHeight.set(newValue);
    }
    get isPanelShow() {
        return this.__isPanelShow.get();
    }
    set isPanelShow(newValue) {
        this.__isPanelShow.set(newValue);
    }
    get stateBarHeight() {
        return this.__stateBarHeight.get();
    }
    set stateBarHeight(newValue) {
        this.__stateBarHeight.set(newValue);
    }
    get navigationBarHeight() {
        return this.__navigationBarHeight.get();
    }
    set navigationBarHeight(newValue) {
        this.__navigationBarHeight.set(newValue);
    }
    get theme() {
        return this.__theme.get();
    }
    set theme(newValue) {
        this.__theme.set(newValue);
    }
    aboutToAppear() {
        helpers_1.showToastIfNeeded();
        this.initGoal().then(() => {
            console.log('aboutToAppear initGoal begin');
            const displaySize = helpers_1.getDefaultDisplaySizeInVP();
            this.isLoading = false;
            console.log(`display height: ${displaySize.height.toString()}`);
            setTimeout(() => {
                console.log('aboutToAppear setTimeout begin');
                let attrs = new ComponentInspector_1.default('flag-calendar-page-container');
                this.panelHalfHeight =
                    displaySize.height - px2vp(this.stateBarHeight) - px2vp(this.navigationBarHeight) - attrs.getBottom() + 24;
                console.log(`flag-calendar-page-container: attrs height: ${attrs.getHeight().toString()}`);
                this.isPanelShow = true;
                console.log('aboutToAppear setTimeout end');
            }, 200);
            console.log('aboutToAppear initGoal end');
        });
    }
    aboutToDisappear() {
        this.isLoading = true;
    }
    async initGoal() {
        console.log('initGoal begin');
        const addedGoals = await cloudApis_1.default.getGoalList();
        addedGoals.forEach((goal) => {
            if (goal.goalType === this.goalType) {
                this.goal = goal;
            }
        });
        this.numOfSignedDays = this.goal.recordDaysTimeStamps.length;
        this.numOfContinuousSignedDays = this.continuousDays(this.goal.recordDaysTimeStamps);
        console.log('initGoal end');
    }
    continuousDays(dayTimestamps) {
        const len = dayTimestamps.length;
        const newArray = dayTimestamps.sort((a, b) => a - b);
        const numOfMillisecondOneDay = 86400000;
        let dayNum = len > 0 ? 1 : 0;
        for (let i = len - 1; i > 0; i--) {
            if (newArray[i] - newArray[i - 1] === numOfMillisecondOneDay) {
                dayNum++;
            }
            else {
                break;
            }
        }
        return dayNum;
    }
    NavigationIcons(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //      Image($r('app.media.share'))
            Image.create(ThemeGet_1.getTheme(this.theme).IC_PUBLIC_SHARE);
            //      Image($r('app.media.share'))
            Image.width(24);
            //      Image($r('app.media.share'))
            Image.height(24);
            //      Image($r('app.media.share'))
            Image.objectFit(ImageFit.Contain);
            //      Image($r('app.media.share'))
            Image.margin(10);
            //      Image($r('app.media.share'))
            Image.onClick(() => {
                _ohos_router_1.pushUrl({
                    url: 'pages/share-preview',
                    params: {
                        goal: this.goal,
                        activeNum: this.activeNum,
                    }
                });
            });
            if (!isInitialRender) {
                //      Image($r('app.media.share'))
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //      Image($r('app.media.ic_public_more'))
            Image.create(ThemeGet_1.getTheme(this.theme).IC_PUBLIC_MORE);
            //      Image($r('app.media.ic_public_more'))
            Image.width(26);
            //      Image($r('app.media.ic_public_more'))
            Image.height(26);
            //      Image($r('app.media.ic_public_more'))
            Image.objectFit(ImageFit.Contain);
            //      Image($r('app.media.ic_public_more'))
            Image.margin(12);
            //      Image($r('app.media.ic_public_more'))
            Image.bindMenu([
                {
                    value: '修改目标',
                    action: () => {
                        _ohos_router_1.pushUrl({
                            url: 'pages/modify-target',
                            params: {
                                goal: this.goal,
                                isAddingTarget: false
                            }
                        });
                    }
                },
                {
                    value: '删除目标',
                    action: () => {
                        AlertDialog.show({
                            message: '是否删除年度目标',
                            //message:`是否删除年度${this.goal.goalName}目标`,
                            alignment: DialogAlignment.Bottom,
                            offset: { dx: 0, dy: -12 },
                            primaryButton: {
                                value: '取消',
                                fontColor: 0xfb6522,
                                action: () => {
                                }
                            },
                            secondaryButton: {
                                value: '删除',
                                action: async () => {
                                    const res = await cloudApis_1.default.delGoal(ObservedObject.GetRawObject(this.goal));
                                    if (res.resultCode != 0) {
                                        console.error(`del goal ${this.goal.id} failed: errorcode : ${res.resultCode}`);
                                        return;
                                    }
                                    helpers_1.setNextPageToastMessage('目标删除成功');
                                    _ohos_router_1.pushUrl({ url: 'pages/index' }, _ohos_router_1.RouterMode.Single);
                                    _ohos_router_1.clear();
                                },
                            }
                        });
                    }
                }
            ]);
            //      Image($r('app.media.ic_public_more'))
            Image.alignSelf(ItemAlign.End);
            if (!isInitialRender) {
                //      Image($r('app.media.ic_public_more'))
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
    }
    NavigationTitle(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('年度');
            Text.fontSize(20);
            Text.fontWeight(FontWeight.Medium);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(this.goal.goalName);
            Text.fontSize(20);
            Text.fontWeight(FontWeight.Medium);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('目标');
            Text.fontSize(20);
            Text.fontWeight(FontWeight.Medium);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
    }
    Page(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.margin({ top: 40 });
            Column.id('flag-calendar-page-container');
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (!this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new ProgressCircle_1.ProgressCircle(this, {
                                    bgStartAngle: this.goal.isWeight ? Math.PI * 1 / 4 : 0,
                                    bgEndAngle: this.goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 2,
                                    arcStartAngle: this.goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2,
                                    arcEndAngle: (this.goal.isWeight ? (Math.PI * 3 / 2) : (Math.PI * 2))
                                        * this.goal.progress + (this.goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2),
                                    startColor: this.goal.color[0],
                                    endColor: this.goal.color[1],
                                    isCounterClockWise: this.goal.isWeight,
                                    //TODO: 硬编码 canvas大小
                                    canvasSize: 250,
                                    slot: () => {
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Column.create();
                                            if (!isInitialRender) {
                                                Column.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Image.create(this.goal.icon);
                                            Image.width(50);
                                            Image.aspectRatio(1);
                                            Image.margin({ bottom: 8 });
                                            if (!isInitialRender) {
                                                Image.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Row.create();
                                            Row.alignItems(VerticalAlign.Bottom);
                                            if (!isInitialRender) {
                                                Row.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Text.create(`${(this.goal.progress * 100).toFixed()}`);
                                            Text.fontSize(36);
                                            Text.fontWeight(500);
                                            if (!isInitialRender) {
                                                Text.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        Text.pop();
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            If.create();
                                            if (!this.goal.isWeight) {
                                                this.ifElseBranchUpdateFunction(0, () => {
                                                    this.observeComponentCreation((elmtId, isInitialRender) => {
                                                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                        Text.create('%');
                                                        Text.fontSize(16);
                                                        Text.margin({ left: 2, bottom: 7 });
                                                        if (!isInitialRender) {
                                                            Text.pop();
                                                        }
                                                        ViewStackProcessor.StopGetAccessRecording();
                                                    });
                                                    Text.pop();
                                                });
                                            }
                                            else {
                                                If.branchId(1);
                                            }
                                            if (!isInitialRender) {
                                                If.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        If.pop();
                                        Row.pop();
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Text.create(`${this.goal.completeValue} / ${this.goal.goalValue} ${this.goal.unit}`);
                                            Text.fontSize(14);
                                            Text.fontWeight(500);
                                            if (!isInitialRender) {
                                                Text.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        Text.pop();
                                        Column.pop();
                                    }
                                }, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(helpers_1.getTimeRange());
            Text.margin({ left: 24, right: 24, bottom: 4 });
            Text.fontSize(16);
            Text.margin({ top: 32 });
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777242, "type": 10004, params: [this.activeNum, helpers_1.numberWithCommas(this.activeNum)], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Text.opacity(0.6);
            Text.fontSize(14);
            Text.margin({ left: 24, right: 14, bottom: 4, top: 4 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            RowSplit.create();
            RowSplit.margin({ top: 24, right: 4, left: 4 });
            RowSplit.padding(12);
            RowSplit.borderRadius(16);
            RowSplit.backgroundColor(ThemeGet_1.getTheme(this.theme).CARD_PANEL_BACKGROUND);
            RowSplit.width('90%');
            RowSplit.height(76);
            RowSplit.onClick(() => {
                this.isPanelShow = !this.isPanelShow;
            });
            if (!isInitialRender) {
                RowSplit.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.height('100%');
            Column.width('50%');
            Column.justifyContent(FlexAlign.Center);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('累计打卡');
            Text.opacity(0.6);
            Text.fontWeight('blod');
            Text.fontSize(12);
            Text.margin({ bottom: 4 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${this.numOfSignedDays}天`);
            Text.fontSize(20);
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.height('100%');
            Column.width('50%');
            Column.justifyContent(FlexAlign.Center);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('连续打卡');
            Text.opacity(0.6);
            Text.fontWeight('blod');
            Text.fontSize(12);
            Text.margin({ bottom: 4 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${this.numOfContinuousSignedDays}天`);
            Text.fontSize(20);
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        RowSplit.pop();
        Column.pop();
    }
    CalendarCircle(signed, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Circle.create({ width: 10, height: 10 });
            Circle.fill(signed ? 0xff841a : 0xd8d8d8);
            Circle.opacity(0.6);
            if (!isInitialRender) {
                Circle.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.backgroundColor(ThemeGet_1.getTheme(this.theme).BACKGROUND_MAIN);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Navigation.create();
            Navigation.menus({ builder: () => {
                    this.NavigationIcons.call(this);
                } });
            Navigation.size({ width: '100%', height: '100%' });
            Navigation.titleMode(NavigationTitleMode.Mini);
            Navigation.hideBackButton(false);
            Navigation.title({ builder: () => {
                    this.NavigationTitle.call(this);
                } });
            if (!isInitialRender) {
                Navigation.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        LoadingProgress.create();
                        LoadingProgress.width('20%');
                        if (!isInitialRender) {
                            LoadingProgress.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.Page.bind(this)();
        Navigation.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Panel.create(this.isPanelShow);
            Panel.type(PanelType.Temporary);
            Panel.mode(PanelMode.Half);
            Panel.dragBar(true);
            Panel.halfHeight(this.panelHalfHeight);
            Panel.fullHeight(320);
            Panel.backgroundColor(ThemeGet_1.getTheme(this.theme).CARD_PANEL_BACKGROUND);
            Panel.backdropBlur(0);
            Panel.hitTestBehavior(HitTestMode.Transparent);
            Panel.onHeightChange((height) => {
                this.panelMaskColor = px2vp(height) > this.panelHalfHeight + 10 ? 'rgba(0,0,0,0.2)' : '';
            });
            if (!isInitialRender) {
                Panel.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.alignItems(HorizontalAlign.Center);
            Column.padding({ left: 20, right: 20, top: 8 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            Row.alignItems(VerticalAlign.Bottom);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('打卡日历');
            Text.fontSize(16);
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.CalendarCircle.bind(this)(true);
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('已打卡');
            Text.margin({ left: 6, right: 8 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.CalendarCircle.bind(this)(false);
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('未打卡');
            Text.margin({ left: 6, right: 8 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            Row.margin({ top: 12 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (!this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new SignCalendar_1.SignCalendar(this, { recordDaysTimeStamps: this.goal.recordDaysTimeStamps }, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Row.pop();
        Column.pop();
        Panel.pop();
        Stack.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new FlagCalendar(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		var commonCachedModule = globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] ? globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId]: null;
/******/ 		if (commonCachedModule) { return commonCachedModule.exports; }
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		function isCommonModue(moduleId) {
/******/ 		                if (globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"]) {
/******/ 		                  const length = globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"].length;
/******/ 		                  switch (length) {
/******/ 		                    case 1:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId];
/******/ 		                    case 2:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId] ||
/******/ 		                      globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][1][1][moduleId];
/******/ 		                  }
/******/ 		                }
/******/ 		                return undefined;
/******/ 		              }
/******/ 		if (globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] && String(moduleId).indexOf("?name=") < 0 && isCommonModue(moduleId)) {
/******/ 		  globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId] = module;
/******/ 		}
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/flag-calendar.ets?entry");
/******/ 	_5047ee604ae08900cdea13aa257f5259 = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=flag-calendar.js.map