var _5047ee604ae08900cdea13aa257f5259;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/Application/AbilityStage.ts?entry":
/*!***********************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/Application/AbilityStage.ts?entry ***!
  \***********************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var _ohos_hilog_1  = globalThis.requireNapi('hilog');
var _ohos_app_ability_AbilityStage_1  = globalThis.requireNapi('app.ability.AbilityStage');
// import abilityManager from '@ohos.app.ability.abilityManager';
//验证一下厂商设备信息接口
var _ohos_deviceInfo_1  = globalThis.requireNapi('deviceInfo');
class MyAbilityStage extends _ohos_app_ability_AbilityStage_1 {
    onCreate() {
        var _a;
        _ohos_hilog_1.isLoggable(0x0000, 'testTag', _ohos_hilog_1.LogLevel.INFO);
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'AbilityStage onCreate');
        //这里测试一下厂商信息能否正确显示，通过deviceInfo查询系统版本
        _ohos_hilog_1.info(0x0000, 'testTag1', 'Succeeded in loading the content. Data: %{public}s', (_a = _ohos_deviceInfo_1.osFullName) !== null && _a !== void 0 ? _a : '');
        //在板子上面模拟深夜模式
        // abilityManager.updateConfiguration({ colorMode:1 }, () => {
        //     hilog.info(0x0000, 'testTag10', 'Succeeded in loading the content. Data: %{public}s',  '');
        // })
    }
    //这个函数可以监听到应用级变化，发生全局配置变更时触发回调，当前全局配置包括系统语言、深浅色模式。
    onConfigurationUpdate(config) {
        var _a;
        _ohos_hilog_1.info(0x0000, 'testTag2', 'Succeeded in loading the content. Data: %{public}s', (_a = JSON.stringify(config)) !== null && _a !== void 0 ? _a : '');
        let color = config.colorMode;
        AppStorage.SetAndLink("theme", color);
    }
}
globalThis.exports.default = MyAbilityStage;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/Application/AbilityStage.ts?entry"](0, __webpack_exports__);
/******/ 	_5047ee604ae08900cdea13aa257f5259 = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=AbilityStage.js.map