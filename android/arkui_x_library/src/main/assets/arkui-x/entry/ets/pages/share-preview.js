var _5047ee604ae08900cdea13aa257f5259;
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets":
/*!******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var _ohos_arkui_componentSnapshot_1  = globalThis.requireNapi('arkui.componentSnapshot');
__webpack_require__(/*! @ohos.multimedia.image */ "../../api/@ohos.multimedia.image.d.ts");
__webpack_require__(/*! @ohos.util */ "../../api/@ohos.util.d.ts");
class ComponentSnapshot {
    //  async getBase64(id: string): Promise<string> {
    //    let pixel: image.PixelMap = await componentSnapshot.get(id);
    //    await pixel.scale(0.5,0.5);
    //    let arrayBuffer: ArrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
    //    pixel.readPixelsToBuffer(arrayBuffer);
    //    let unit8Array = new Uint8Array(arrayBuffer);
    //    console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
    //    let base64Helper = new util.Base64Helper();
    //    let base64Str = base64Helper.encodeToStringSync(unit8Array)
    //    console.log('base64Str.length =' + base64Str.substring(0,10));
    //    return base64Str;
    //    //return 'base64Str';
    //  }
    async getBase64(id) {
        let pixel = await _ohos_arkui_componentSnapshot_1.get(id);
        let arrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
        pixel.readPixelsToBuffer(arrayBuffer);
        let unit8Array = new Uint8Array(arrayBuffer);
        console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
        return unit8Array;
        //    let pixel: image.PixelMap = await componentSnapshot.get(id);
        //    let arrayBuffer: ArrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
        //    pixel.readPixelsToBuffer(arrayBuffer);
        //    let unit8Array = new Uint8Array(arrayBuffer);
        //    console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
        //    let base64Helper = new util.Base64Helper();
        //    let base64Str = base64Helper.encodeToStringSync(unit8Array)
        //    console.log('base64Str.length = ComponentSnapshot ' + base64Str.length);
        //    for (let i = 0; i < base64Str.length / 1000; i++) {
        //      console.log('base64Str.length = ComponentSnapshot ' + base64Str.substr(i * 1000, 1000));
        //    }
        //    console.log('base64Str.length = ComponentSnapshot ' + base64Str.substr(base64Str.length / 1000 * 1000, base64Str.length - base64Str.length / 1000 * 1000));
        //    return base64Str;
        //return 'base64Str';
    }
}
exports["default"] = ComponentSnapshot;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getTheme = void 0;
const ThemeConst_1 = __webpack_require__(/*! ../model/ThemeConst */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets");
// 根据全局变量theme来控制模式
function getTheme(theme) {
    return ThemeConst_1.DefaultTheme;
}
exports.getTheme = getTheme;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/9/24
  my_annual_flag: 云侧接口调用
 */
var _ohos_net_http_1  = globalThis.requireNapi('net.http');
const mockData_1 = __webpack_require__(/*! ./mockData */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets");
const Goals_1 = __webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const isMock = true;
const mockDelay = 1000;
const achievementDomain = 'https://lfhealthtest2.hwcloudtest.cn:18444/achievement';
//const huid = '4130086000005055643';
// 13019409302
const huid = '10086000011925744';
const commonBody = {
    appId: 'com.huawei.health',
    appType: 1,
    deviceId: '4fe5240a56f37a11',
    deviceType: 9,
    // token: '0413008600000505564356624a402cc253ef201f90c7230bd25b5060d2e466b1e85fa939d999cd3e7152',
    // 13019409302
    token: '00010086000011925744fdf226b8bfbfefa73011ecca54a0819cd50d0d18ab008273c6e8aa7c86342cda',
    tokenType: 1,
    ts: new Date().getTime(),
};
class CloudApis {
    //  options: {extraData: {}}
    constructor() {
        this.domain = achievementDomain;
        this.options = {
            method: _ohos_net_http_1.RequestMethod.POST,
            extraData: commonBody,
            header: {
                'Content-Type': 'application/json',
                'x-huid': huid
            },
            readTimeout: 10000,
            connectTimeout: 10000
        };
    }
    // 获取 6 种目标的统计数据（如现激活人数等）
    async getGoalStatistics() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalStatistics.result);
            //}, mockDelay))
            resResultObj = mockData_1.mockCloudData.getGoalStatistics.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalStatistics`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const todoGoals = resResultObj.statistics.map((e) => {
            return new Goals_1.TodoGoal(e.goalType, e.activeNum);
        });
        return todoGoals;
    }
    // 获取本用户激活的目标的各种数据（如已完成数值、目标数值等）
    async getGoalList() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalList.result);
            //}, mockDelay));
            resResultObj = mockData_1.mockCloudData.getGoalList.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalList}`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const addedGoals = resResultObj.goal.map((e) => {
            return new Goals_1.AddedGoal(e.id, e.goalType, e.completeValue, e.goalValue, e.currentWeight, e.recordDays);
        });
        return addedGoals;
    }
    // 添加新目标（除了体重目标）
    async addGoal(goalType, goalValue) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: goalType, goalValue: goalValue, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 添加体重目标
    async addWeightGoal(initWeight, targetWeight) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: Goals_1.GoalType.Weight, goalValue: targetWeight, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addWightGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 删除目标
    async delGoal(addedGoal) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.delGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ records: [addedGoal.id] }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/delGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: delGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 修改编辑目标
    async updateGoal(addedGoal, goalValue, currentWeight = 0) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.updateGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, id: addedGoal.id, goalValue: goalValue, frequency: 0, unitValue: 0, currentWeight: 0 }, commonBody);
            if (addedGoal.goalType === Goals_1.GoalType.Weight) {
                body.currentWeight = currentWeight;
            }
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/updateGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: updateGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
}
exports["default"] = new CloudApis();


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets":
/*!*******************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
const consts = {
    // TODO: 硬编码状态栏高度，单位 vp。（未知，估计的，待新版本提供接口）
    STATUS_BAR_HEIGHT: 24,
    // TODO: 硬编码导航栏高度，单位 vp。（通过 getInspectorByKey 方式间接获取的，暂不知道如何直接获取导航栏高度，故硬编码）
    NAV_BAR_HEIGHT: 40,
};
exports["default"] = consts;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets":
/*!********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getDefaultDisplaySizeInVP = exports.setNextPageToastMessage = exports.showToastIfNeeded = exports.getTimeRange = exports.numberWithCommas = exports.maxDecimals = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/13
  my_annual_flag: 工具函数
 */
var _ohos_prompt_1  = globalThis.requireNapi('prompt');
var _ohos_display_1  = globalThis.requireNapi('display');
/**
 * 让数字最多保留 x 位小鼠
 * @param num 需要简化的数字
 * @param maxDecimals 最多保留的小数位数
 */
function maxDecimals(num, maxDecimals) {
    return Math.round(num * Math.pow(10, maxDecimals)) / Math.pow(10, maxDecimals);
}
exports.maxDecimals = maxDecimals;
/**
 * 给数字每千位添加一个逗号
 * @param x 数字
 * @return 添加过逗号的字符串
 */
function numberWithCommas(x) {
    return maxDecimals(x, 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
exports.numberWithCommas = numberWithCommas;
/**
 * 返回调用时当年的第一天和最后一天
 * @return 2.g. '2022/01/01-2022/12/31'
 */
function getTimeRange() {
    const date = new Date();
    const currentYear = date.getFullYear();
    const startDate = `${currentYear}/01/01`;
    const endDate = `${currentYear}12/31`;
    return `${startDate}-${endDate}`;
}
exports.getTimeRange = getTimeRange;
// 放在 onPageShow, 在页面启动时，根据是否设置 toast 字串来决定是否展示 toast
function showToastIfNeeded() {
    const msg = AppStorage.Get('toastMessage');
    if (msg) {
        _ohos_prompt_1.showToast({ message: msg });
    }
    AppStorage.SetOrCreate('toastMessage', '');
}
exports.showToastIfNeeded = showToastIfNeeded;
/**
 * 设置 toast 信息，并在下次切换页面时显示
 * @param msg 需要显示的文字
 */
function setNextPageToastMessage(msg) {
    AppStorage.SetOrCreate('toastMessage', msg);
}
exports.setNextPageToastMessage = setNextPageToastMessage;
/**
 * 获得默认显示屏的屏幕大小，vp 为单位
 * @return { width: xx, height: xx }
 */
function getDefaultDisplaySizeInVP() {
    console.log('start getDefaultDisplaySync');
    const defaultDisplay = _ohos_display_1.getDefaultDisplaySync();
    console.log('getDefaultDisplaySync' + defaultDisplay);
    return {
        width: px2vp(defaultDisplay.width),
        height: px2vp(defaultDisplay.height),
    };
    //  return {
    //    width: 360,
    //    height: 740,
    //  };
}
exports.getDefaultDisplaySizeInVP = getDefaultDisplaySizeInVP;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.signedDates = exports.mockCloudData = void 0;
exports.mockCloudData = {
    // 获得所有参与者的目标统计数据（坚持人数，完成人数 等...）
    getGoalStatistics: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "statistics": [
                {
                    "goalType": 0,
                    "createNum": 3801,
                    "activeNum": 1367,
                    "completeNum": 1243,
                    "delNum": 906,
                    "timeoutNum": 285
                },
                {
                    "goalType": 1,
                    "createNum": 3309,
                    "activeNum": 897,
                    "completeNum": 1241,
                    "delNum": 886,
                    "timeoutNum": 285
                },
                {
                    "goalType": 2,
                    "createNum": 1436,
                    "activeNum": 838,
                    "completeNum": 14,
                    "delNum": 143,
                    "timeoutNum": 441
                },
                {
                    "goalType": 3,
                    "createNum": 916,
                    "activeNum": 847,
                    "completeNum": 0,
                    "delNum": 67,
                    "timeoutNum": 2
                },
                {
                    "goalType": 4,
                    "createNum": 1964,
                    "activeNum": 474,
                    "completeNum": 1249,
                    "delNum": 240,
                    "timeoutNum": 1
                },
                {
                    "goalType": 5,
                    "createNum": 906,
                    "activeNum": 833,
                    "completeNum": 0,
                    "delNum": 72,
                    "timeoutNum": 1
                }
            ]
        }
    },
    getGoalList: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "goal": [
                {
                    "completeValue": 0,
                    "createTime": 1658217778708,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 1,
                    "goalValue": 1234,
                    "id": 23557,
                    "modifyTime": 1663845225603,
                    "recordDays": [],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 120.197,
                    "createTime": 1658217813686,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 3,
                    "goalValue": 1234,
                    "id": 23559,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 55.4,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 4,
                    "goalValue": 52.1,
                    "id": 23560,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 1,
                    "unitValue": 0
                },
                {
                    "completeValue": 1153834,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 2,
                    "goalValue": 1234567,
                    "id": 23561,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                }
            ]
        }
    },
    addGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    delGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    updateGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    }
};
/**
 * 生成数个今年的随机日期
 * @param numOfDays 需要随机生成日期的数量
 * 返回随机日期数组（unix 时间戳）
 */
const generateRandomDates = function (numOfDays) {
    const currentYear = new Date().getFullYear();
    const firstDayOfYear = new Date(currentYear, 0, 1);
    const lastDayOfYear = new Date(currentYear, 11, 31);
    const res = new Set();
    for (let i = 0; i < numOfDays; i++) {
        let unique = false;
        while (!unique) {
            const randomDate = new Date(firstDayOfYear.getTime() + Math.random() * (lastDayOfYear.getTime() - firstDayOfYear.getTime())).toDateString();
            if (!res.has(randomDate)) {
                res.add(randomDate);
                unique = true;
            }
        }
    }
    return res;
};
exports.signedDates = generateRandomDates(240);


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/17
  my_annual_flag: 颜色对象
 */
class Color {
    constructor(r, g, b) {
        this.r = 0;
        this.g = 0;
        this.b = 0;
        this.r = Math.round(r);
        this.g = Math.round(g);
        this.b = Math.round(b);
    }
    toHex() {
        return `#${this.num2Hex(this.r)}${this.num2Hex(this.g)}${this.num2Hex(this.b)}`;
    }
    toRGB() {
        return `rgb(${this.r}, ${this.g}, ${this.b})`;
    }
    num2Hex(n) {
        const hex = n.toString(16);
        return hex.length == 1 ? `0${hex}}` : hex;
    }
}
exports["default"] = Color;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AddedGoal = exports.TodoGoal = exports.GoalType = void 0;
const Color_1 = __importDefault(__webpack_require__(/*! ./Color */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets"));
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
var GoalType;
(function (GoalType) {
    GoalType[GoalType["Running"] = 0] = "Running";
    GoalType[GoalType["Fitness"] = 1] = "Fitness";
    GoalType[GoalType["Walking"] = 2] = "Walking";
    GoalType[GoalType["Cycling"] = 3] = "Cycling";
    GoalType[GoalType["Weight"] = 4] = "Weight";
    GoalType[GoalType["Swimming"] = 5] = "Swimming";
})(GoalType = exports.GoalType || (exports.GoalType = {}));
const goalsResourceMap = [
    {
        name: { "id": 16777224, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777291, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777255, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777246, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777283, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起奔跑，最美的风景是下一公里！`,
        unit: '公里'
    },
    {
        name: { "id": 16777223, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777288, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777252, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777245, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777282, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，享受流汗的快乐！`,
        unit: '分钟'
    },
    {
        name: { "id": 16777226, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(255, 163, 51), new Color_1.default(255, 133, 25)],
        imgSrc: { "id": 16777276, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777253, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777244, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777286, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起向前，走向广阔世界！`,
        unit: '步'
    },
    {
        name: { "id": 16777222, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777292, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777254, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777247, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777280, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起骑行，战胜风，也享受风！`,
        unit: '公里'
    },
    {
        name: { "id": 16777227, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777293, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777274, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777248, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777281, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，将自律进行到底！`,
        unit: '公斤'
    },
    {
        name: { "id": 16777225, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777295, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777256, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777249, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777284, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，水的阻力，不及你的毅力！`,
        unit: '公里'
    }
];
class TodoGoal {
    constructor(goalType, activeNum) {
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.image = goalsResourceMap[this.goalType].imgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.addImage = goalsResourceMap[this.goalType].addImgSrc;
        this.activeNum = activeNum;
        this.slogan = goalsResourceMap[this.goalType].slogan(helpers_1.numberWithCommas(activeNum));
    }
}
exports.TodoGoal = TodoGoal;
// 已被用户添加并且跟踪目标
class AddedGoal {
    constructor(id, goalType, completeValue, goalValue, initWeight, recordDaysTimeStamps) {
        this.id = id;
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.icon = goalsResourceMap[this.goalType].iconSrc;
        this.shareImg = goalsResourceMap[this.goalType].shareImgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.completeValue = completeValue;
        this.goalValue = goalValue;
        this.color = goalsResourceMap[this.goalType].color;
        this.initWeight = initWeight;
        this.isWeight = this.goalType === 4;
        this.recordDaysTimeStamps = recordDaysTimeStamps;
        this.progress = this.isWeight ? this.completeValue / this.goalValue : this.completeValue / this.goalValue;
        this.progress = this.isWeight
            ? computeWeightProgress(this.initWeight, this.completeValue, this.goalValue)
            : computeProgress(0, this.completeValue, this.goalValue);
    }
}
exports.AddedGoal = AddedGoal;
// 计算目标完成进度
function computeProgress(start, curr, target) {
    const res = (curr - start) / (target - start);
    if (res < 0)
        return 0;
    if (res >= 0.99 && res < 0.9995)
        return 0.99;
    if (res >= 0.9995)
        return 1;
    return res;
}
// 计算体重目标完成进度
function computeWeightProgress(start, curr, target) {
    if (curr === 0) {
        return 0;
    }
    if (target > start) {
        if (curr <= start) {
            return 0;
        }
        if (curr >= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else if (target < start) {
        if (curr <= start) {
            return 0;
        }
        if (curr <= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else {
        return 1;
    }
}


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DefaultTheme = void 0;
//所有能配置主题的相关资源
//// BACKGROUND_CARD 卡片背景颜色
//// BACKGROUND_MAIN 主页背景色
//// FONT_COLOR_MAIN 字体背景颜色
////CARD_PANEL_BACKGROUND panel弹窗背景颜色（日历，设定目标）
//// START_WINDOW_BACKGROUND ??
//// IC_BACK 返回图标
//// IC_PUBLIC_DEL 删除图标
//// IC_PUBLIC_DET 告警（详情）图标
//// IC_PUBLIC_EDIT 修改图标
//// IC_PUBLIC_HIS 历史图标
//// IC_PUBLIC_MORE 修改删除图标
//// IC_PUBLIC_SHARE 分享图标
//// 系统主题 包括 深浅模式 通过dark，light目录实现
class DefaultTheme {
}
exports.DefaultTheme = DefaultTheme;
DefaultTheme.BACKGROUND_CARD = { "id": 16777238, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BACKGROUND_MAIN = { "id": 16777239, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.FONT_COLOR_MAIN = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.START_WINDOW_BACKGROUND = { "id": 16777241, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.CARD_PANEL_BACKGROUND = { "id": 16777237, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BIG_BTN_BACKGROUND_COLOR = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_BACK = { "id": 16777257, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DEL = { "id": 16777262, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DET = { "id": 16777264, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_EDIT = { "id": 16777266, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_HIS = { "id": 16777268, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_MORE = { "id": 16777270, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_SHARE = { "id": 16777299, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/share-preview.ets?entry":
/*!*******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/share-preview.ets?entry ***!
  \*******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/24
  my_annual_flag: 截图分享预览页
 */
var _ohos_router_1  = globalThis.requireNapi('router');
const Goals_1 = __webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
__webpack_require__(/*! ../common/consts */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/consts.ets");
const ComponentSnapshot_1 = __importDefault(__webpack_require__(/*! ../common/ComponentSnapshot */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets"));
const cloudApis_1 = __importDefault(__webpack_require__(/*! ../common/cloudApis */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets"));
const ThemeGet_1 = __webpack_require__(/*! ../common/ThemeGet */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets");
class SharePreview extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__goal = new ObservedPropertyObjectPU(new Goals_1.AddedGoal(0, 0, 0, 0, 0, []), this, "goal");
        this.__activeNum = new ObservedPropertySimplePU(0, this, "activeNum");
        this.__isShareAll = new ObservedPropertySimplePU(false, this, "isShareAll");
        this.__addedGoals = new ObservedPropertyObjectPU([], this, "addedGoals");
        this.__todoGoals = new ObservedPropertyObjectPU([], this, "todoGoals");
        this.__isLoading = new ObservedPropertySimplePU(false, this, "isLoading");
        this.__theme = this.createStorageLink('theme', -1, "theme");
        this.initGoal = async () => {
            const requests = await Promise.all([cloudApis_1.default.getGoalStatistics(), cloudApis_1.default.getGoalList()]);
            this.todoGoals = requests[0];
            this.addedGoals = requests[1];
            this.isLoading = false;
        };
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.goal !== undefined) {
            this.goal = params.goal;
        }
        if (params.activeNum !== undefined) {
            this.activeNum = params.activeNum;
        }
        if (params.isShareAll !== undefined) {
            this.isShareAll = params.isShareAll;
        }
        if (params.addedGoals !== undefined) {
            this.addedGoals = params.addedGoals;
        }
        if (params.todoGoals !== undefined) {
            this.todoGoals = params.todoGoals;
        }
        if (params.isLoading !== undefined) {
            this.isLoading = params.isLoading;
        }
        if (params.initGoal !== undefined) {
            this.initGoal = params.initGoal;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__goal.purgeDependencyOnElmtId(rmElmtId);
        this.__activeNum.purgeDependencyOnElmtId(rmElmtId);
        this.__isShareAll.purgeDependencyOnElmtId(rmElmtId);
        this.__addedGoals.purgeDependencyOnElmtId(rmElmtId);
        this.__todoGoals.purgeDependencyOnElmtId(rmElmtId);
        this.__isLoading.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__goal.aboutToBeDeleted();
        this.__activeNum.aboutToBeDeleted();
        this.__isShareAll.aboutToBeDeleted();
        this.__addedGoals.aboutToBeDeleted();
        this.__todoGoals.aboutToBeDeleted();
        this.__isLoading.aboutToBeDeleted();
        this.__theme.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get goal() {
        return this.__goal.get();
    }
    set goal(newValue) {
        this.__goal.set(newValue);
    }
    get activeNum() {
        return this.__activeNum.get();
    }
    set activeNum(newValue) {
        this.__activeNum.set(newValue);
    }
    get isShareAll() {
        return this.__isShareAll.get();
    }
    set isShareAll(newValue) {
        this.__isShareAll.set(newValue);
    }
    get addedGoals() {
        return this.__addedGoals.get();
    }
    set addedGoals(newValue) {
        this.__addedGoals.set(newValue);
    }
    get todoGoals() {
        return this.__todoGoals.get();
    }
    set todoGoals(newValue) {
        this.__todoGoals.set(newValue);
    }
    get isLoading() {
        return this.__isLoading.get();
    }
    set isLoading(newValue) {
        this.__isLoading.set(newValue);
    }
    get theme() {
        return this.__theme.get();
    }
    set theme(newValue) {
        this.__theme.set(newValue);
    }
    aboutToAppear() {
        if (_ohos_router_1.getParams() && _ohos_router_1.getParams()['goal']) {
            this.isShareAll = false;
            this.goal = _ohos_router_1.getParams()['goal'];
            this.activeNum = _ohos_router_1.getParams()['activeNum'];
        }
        else {
            this.isLoading = true;
            this.isShareAll = true;
            this.initGoal();
        }
        setTimeout(() => {
            let snap = new ComponentSnapshot_1.default();
            snap.getBase64('share_preview').then((base64Str) => {
                //        console.log('base64Str.length =' + base64Str.substring(0,150));
            });
        }, 1500);
    }
    getActiveNum(goalType) {
        let activeNum = -1;
        this.todoGoals.forEach((goal) => {
            if (goal.goalType == goalType) {
                activeNum = goal.activeNum;
            }
        });
        return activeNum;
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Navigation.create();
            Navigation.size({ width: '100%', height: '100%' });
            Navigation.title({ builder: () => {
                    this.NavigationTitle.call(this);
                } });
            Navigation.hideBackButton(false);
            Navigation.titleMode(NavigationTitleMode.Mini);
            Navigation.backgroundColor(ThemeGet_1.getTheme(this.theme).BACKGROUND_MAIN);
            if (!isInitialRender) {
                Navigation.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.padding({ left: 24, right: 24 });
            Column.alignItems(HorizontalAlign.Center);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        LoadingProgress.create();
                        LoadingProgress.width('20%');
                        LoadingProgress.offset({ y: '-10%' });
                        if (!isInitialRender) {
                            LoadingProgress.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.borderRadius(16);
            Column.backgroundColor(0xf5f5f5);
            Column.shadow({ radius: 50, color: 'rgba(0,0,0,0.1)', offsetX: -4, offsetY: 9 });
            Column.id('share_preview');
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.padding(12);
            Column.width('100%');
            Column.height('100%');
            Column.aspectRatio(this.isShareAll ? 0.8 : 1);
            Column.backgroundImage(this.isShareAll ? { "id": 16777279, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" } : this.goal.shareImg);
            Column.backgroundImageSize(ImageSize.Cover);
            Column.border({ radius: { topLeft: 16, topRight: 16 } });
            Column.align(Alignment.TopStart);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create({ space: 4 });
            Row.width('100%');
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 16777277, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Image.width(24);
            Image.height(24);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('大头钉');
            Text.fontSize(10);
            Text.fontFamily('Roboto-Medium');
            Text.fontColor(Color.White);
            Text.margin({ bottom: 2 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (!this.isShareAll) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`${helpers_1.numberWithCommas(this.activeNum)} 人一起坚持`);
                        Text.fontSize(10);
                        Text.fontFamily('PingFangSC-Regular');
                        Text.fontColor(Color.White);
                        Text.opacity(0.6);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Column.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isShareAll) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Column.create();
                        if (!isInitialRender) {
                            Column.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create({ "id": 16777233, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
                        Text.fontWeight(FontWeight.Bold);
                        Text.fontSize(12);
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`${new Date().getFullYear()}.01.01-${new Date().getFullYear()}.12.31`);
                        Text.fontSize(12);
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Column.pop();
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isShareAll) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        If.create();
                        if (this.addedGoals.length > 0) {
                            this.ifElseBranchUpdateFunction(0, () => {
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    Grid.create();
                                    Grid.width('100%');
                                    Grid.columnsTemplate('1fr 1fr');
                                    Grid.columnsGap(10);
                                    Grid.rowsGap(10);
                                    Grid.height((this.addedGoals.length / 2 * 50) + (this.addedGoals.length % 2 * 50));
                                    if (!isInitialRender) {
                                        Grid.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                                this.observeComponentCreation((elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    ForEach.create();
                                    const forEachItemGenFunction = (_item, index) => {
                                        const item = _item;
                                        {
                                            const isLazyCreate =  true && (Grid.willUseProxy() === true);
                                            const itemCreation = (elmtId, isInitialRender) => {
                                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                                GridItem.create(deepRenderFunction, isLazyCreate);
                                                if (!isInitialRender) {
                                                    GridItem.pop();
                                                }
                                                ViewStackProcessor.StopGetAccessRecording();
                                            };
                                            const observedShallowRender = () => {
                                                this.observeComponentCreation(itemCreation);
                                                GridItem.pop();
                                            };
                                            const observedDeepRender = () => {
                                                this.observeComponentCreation(itemCreation);
                                                this.GolItem.bind(this)(index, item.goalName, item.goalValue, item.unit, this.getActiveNum(item.goalType));
                                                GridItem.pop();
                                            };
                                            const deepRenderFunction = (elmtId, isInitialRender) => {
                                                itemCreation(elmtId, isInitialRender);
                                                this.updateFuncByElmtId.set(elmtId, itemCreation);
                                                this.GolItem.bind(this)(index, item.goalName, item.goalValue, item.unit, this.getActiveNum(item.goalType));
                                                GridItem.pop();
                                            };
                                            if (isLazyCreate) {
                                                observedShallowRender();
                                            }
                                            else {
                                                observedDeepRender();
                                            }
                                        }
                                    };
                                    this.forEachUpdateFunction(elmtId, this.addedGoals, forEachItemGenFunction, item => item.id.toString(), true, false);
                                    if (!isInitialRender) {
                                        ForEach.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                });
                                ForEach.pop();
                                Grid.pop();
                            });
                        }
                        else {
                            If.branchId(1);
                        }
                        if (!isInitialRender) {
                            If.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    If.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Row.create();
                        Row.width('100%');
                        Row.alignItems(VerticalAlign.Bottom);
                        if (!isInitialRender) {
                            Row.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Column.create();
                        Column.alignItems(HorizontalAlign.Start);
                        if (!isInitialRender) {
                            Column.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create({ "id": 16777234, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`${helpers_1.maxDecimals(this.goal.progress * 100, 1)}%`);
                        Text.fontWeight(FontWeight.Bold);
                        Text.fontStyle(FontStyle.Italic);
                        Text.fontSize(26);
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Column.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Blank.create();
                        if (!isInitialRender) {
                            Blank.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Blank.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Column.create();
                        Column.alignItems(HorizontalAlign.Start);
                        if (!isInitialRender) {
                            Column.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Row.create();
                        if (!isInitialRender) {
                            Row.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(new Date().getFullYear().toString());
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create({ "id": 16777220, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Row.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`${this.goal.goalValue} ${this.goal.unit}`);
                        Text.fontWeight(FontWeight.Bold);
                        Text.fontStyle(FontStyle.Italic);
                        Text.fontSize(20);
                        Text.fontColor(Color.White);
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                    Column.pop();
                    Row.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create({ space: 8 });
            Row.width('100%');
            Row.padding(12);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 16777251, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Image.width(21);
            Image.height(21);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777231, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Text.opacity(0.6);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Column.pop();
        Column.pop();
        Navigation.pop();
    }
    NavigationTitle(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('预览');
            Text.fontSize(20);
            Text.fontWeight(FontWeight.Medium);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
    }
    GolItem(index, type, target, unit, activeNum, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.alignItems((index % 2 === 0) ? HorizontalAlign.Start : HorizontalAlign.End);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(type);
            Text.fontColor(Color.White);
            Text.fontSize(11);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777230, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Text.fontColor(Color.White);
            Text.fontSize(11);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(target.toString());
            Text.fontColor(Color.White);
            Text.fontSize(16);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(unit);
            Text.fontColor(Color.White);
            Text.fontSize(11);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`与${activeNum}人一起坚持`);
            Text.fontColor(Color.White);
            Text.fontSize(11);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new SharePreview(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();


/***/ }),

/***/ "../../api/@ohos.multimedia.image.d.ts":
/*!*********************************************!*\
  !*** ../../api/@ohos.multimedia.image.d.ts ***!
  \*********************************************/
/***/ (() => {



/***/ }),

/***/ "../../api/@ohos.util.d.ts":
/*!*********************************!*\
  !*** ../../api/@ohos.util.d.ts ***!
  \*********************************/
/***/ (() => {



/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		var commonCachedModule = globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] ? globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId]: null;
/******/ 		if (commonCachedModule) { return commonCachedModule.exports; }
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		function isCommonModue(moduleId) {
/******/ 		                if (globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"]) {
/******/ 		                  const length = globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"].length;
/******/ 		                  switch (length) {
/******/ 		                    case 1:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId];
/******/ 		                    case 2:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId] ||
/******/ 		                      globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][1][1][moduleId];
/******/ 		                  }
/******/ 		                }
/******/ 		                return undefined;
/******/ 		              }
/******/ 		if (globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] && String(moduleId).indexOf("?name=") < 0 && isCommonModue(moduleId)) {
/******/ 		  globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId] = module;
/******/ 		}
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/share-preview.ets?entry");
/******/ 	_5047ee604ae08900cdea13aa257f5259 = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=share-preview.js.map