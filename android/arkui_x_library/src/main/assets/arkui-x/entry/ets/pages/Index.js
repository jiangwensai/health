var _5047ee604ae08900cdea13aa257f5259;
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Channel.ets":
/*!********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Channel.ets ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
//var bridge = globalThis.requireNapi("bridge", true, "com.huawei.health/entry");;
// @ts-ignore
var _ohos_bridge_1  = globalThis.requireNapi('bridge');
__webpack_require__(/*! @ohos.hilog */ "../../api/@ohos.hilog.d.ts");
__webpack_require__(/*! ./MethodData */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/MethodData.ets");
const Logger_1 = __importDefault(__webpack_require__(/*! ./Logger */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Logger.ets"));
const TAG = 'Channel';
class Channel {
    constructor(name) {
        this.name = name;
        this.bridgeIml = _ohos_bridge_1.createBridge(name);
        Logger_1.default.info(TAG, `Bridge name = ${name}`);
    }
    /**
     * ts调用其他平台方法
     * @param method 调用的方法名
     * @param parameters  调用的方法的参数
     */
    callMethod(method, parameters) {
        let label = '';
        let result = '';
        if (parameters === null) {
            this.bridgeIml.callMethod(method).then((data) => {
                label = data;
                result = 'success';
            }).catch((error) => {
                label = JSON.stringify(error);
                result = 'failed';
            });
        }
        else {
            this.bridgeIml.callMethod(method, parameters).then((data) => {
                label = data;
                result = 'success';
            }).catch((error) => {
                label = JSON.stringify(error);
                result = 'failed';
            });
        }
        Logger_1.default.info(TAG, `Call method ${result} , result = ${label}`);
    }
    /**
     * ts发送消息给其他平台
     * @param message 发送的消息
     */
    sendMessage(message) {
        let label = '';
        let result = '';
        this.bridgeIml.sendMessage(message).then((data) => {
            label = data;
            result = 'success';
        }).catch((error) => {
            label = JSON.stringify(error);
            result = 'failed';
        });
        Logger_1.default.info(TAG, `Send message ${result} , result = ${label}`);
    }
    /**
     * 接收其他平台发送的消息
     */
    receiveMessage() {
        let label = '';
        this.bridgeIml.setMessageListener((data) => {
            label = data;
        });
        Logger_1.default.info(TAG, `Receive message , result = ${label}`);
    }
    /**
     * ts注册方法，供其他平台使用
     * @param methodData 注册的方法
     */
    registerMethod(methodData) {
        let label = '';
        let result = '';
        this.bridgeIml.registerMethod(methodData).then((data) => {
            label = data;
            result = 'success';
        }).catch((error) => {
            label = JSON.stringify(error);
            result = 'failed';
        });
        Logger_1.default.info(TAG, `Register method ${result} , name = ${methodData.name} , result = ${label}`);
    }
    /**
     * ts取消注册方法
     * @param methodData 取消注册的方法
     */
    unregisterMethod(methodData) {
        let label = '';
        let result = '';
        this.bridgeIml.unRegisterMethod(methodData).then((data) => {
            label = data;
            result = 'success';
        }).catch((error) => {
            label = JSON.stringify(error);
            result = 'failed';
        });
        Logger_1.default.info(TAG, `Register method ${result} , name = ${methodData.name} , result = ${label}`);
    }
}
exports["default"] = Channel;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets":
/*!******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets ***!
  \******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var _ohos_arkui_componentSnapshot_1  = globalThis.requireNapi('arkui.componentSnapshot');
__webpack_require__(/*! @ohos.multimedia.image */ "../../api/@ohos.multimedia.image.d.ts");
__webpack_require__(/*! @ohos.util */ "../../api/@ohos.util.d.ts");
class ComponentSnapshot {
    //  async getBase64(id: string): Promise<string> {
    //    let pixel: image.PixelMap = await componentSnapshot.get(id);
    //    await pixel.scale(0.5,0.5);
    //    let arrayBuffer: ArrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
    //    pixel.readPixelsToBuffer(arrayBuffer);
    //    let unit8Array = new Uint8Array(arrayBuffer);
    //    console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
    //    let base64Helper = new util.Base64Helper();
    //    let base64Str = base64Helper.encodeToStringSync(unit8Array)
    //    console.log('base64Str.length =' + base64Str.substring(0,10));
    //    return base64Str;
    //    //return 'base64Str';
    //  }
    async getBase64(id) {
        let pixel = await _ohos_arkui_componentSnapshot_1.get(id);
        let arrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
        pixel.readPixelsToBuffer(arrayBuffer);
        let unit8Array = new Uint8Array(arrayBuffer);
        console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
        return unit8Array;
        //    let pixel: image.PixelMap = await componentSnapshot.get(id);
        //    let arrayBuffer: ArrayBuffer = new ArrayBuffer(pixel.getPixelBytesNumber());
        //    pixel.readPixelsToBuffer(arrayBuffer);
        //    let unit8Array = new Uint8Array(arrayBuffer);
        //    console.log('base64Str  unit8Array.byteLength=' + unit8Array.byteLength);
        //    let base64Helper = new util.Base64Helper();
        //    let base64Str = base64Helper.encodeToStringSync(unit8Array)
        //    console.log('base64Str.length = ComponentSnapshot ' + base64Str.length);
        //    for (let i = 0; i < base64Str.length / 1000; i++) {
        //      console.log('base64Str.length = ComponentSnapshot ' + base64Str.substr(i * 1000, 1000));
        //    }
        //    console.log('base64Str.length = ComponentSnapshot ' + base64Str.substr(base64Str.length / 1000 * 1000, base64Str.length - base64Str.length / 1000 * 1000));
        //    return base64Str;
        //return 'base64Str';
    }
}
exports["default"] = ComponentSnapshot;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Logger.ets":
/*!*******************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Logger.ets ***!
  \*******************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _ohos_hilog_1  = globalThis.requireNapi('hilog');
class Logger {
    constructor(prefix) {
        this.format = '%{public}s, %{public}s';
        this.prefix = prefix;
        this.domain = 0xFF00;
    }
    debug(...args) {
        _ohos_hilog_1.debug(this.domain, this.prefix, this.format, args);
    }
    info(...args) {
        _ohos_hilog_1.info(this.domain, this.prefix, this.format, args);
    }
    warn(...args) {
        _ohos_hilog_1.warn(this.domain, this.prefix, this.format, args);
    }
    error(...args) {
        _ohos_hilog_1.error(this.domain, this.prefix, this.format, args);
    }
}
exports["default"] = new Logger('ArkUI-X-Health');


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/MethodData.ets":
/*!***********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/MethodData.ets ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
class MethodData {
}
exports["default"] = MethodData;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getTheme = void 0;
const ThemeConst_1 = __webpack_require__(/*! ../model/ThemeConst */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets");
// 根据全局变量theme来控制模式
function getTheme(theme) {
    return ThemeConst_1.DefaultTheme;
}
exports.getTheme = getTheme;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets ***!
  \**********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/9/24
  my_annual_flag: 云侧接口调用
 */
var _ohos_net_http_1  = globalThis.requireNapi('net.http');
const mockData_1 = __webpack_require__(/*! ./mockData */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets");
const Goals_1 = __webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const isMock = true;
const mockDelay = 1000;
const achievementDomain = 'https://lfhealthtest2.hwcloudtest.cn:18444/achievement';
//const huid = '4130086000005055643';
// 13019409302
const huid = '10086000011925744';
const commonBody = {
    appId: 'com.huawei.health',
    appType: 1,
    deviceId: '4fe5240a56f37a11',
    deviceType: 9,
    // token: '0413008600000505564356624a402cc253ef201f90c7230bd25b5060d2e466b1e85fa939d999cd3e7152',
    // 13019409302
    token: '00010086000011925744fdf226b8bfbfefa73011ecca54a0819cd50d0d18ab008273c6e8aa7c86342cda',
    tokenType: 1,
    ts: new Date().getTime(),
};
class CloudApis {
    //  options: {extraData: {}}
    constructor() {
        this.domain = achievementDomain;
        this.options = {
            method: _ohos_net_http_1.RequestMethod.POST,
            extraData: commonBody,
            header: {
                'Content-Type': 'application/json',
                'x-huid': huid
            },
            readTimeout: 10000,
            connectTimeout: 10000
        };
    }
    // 获取 6 种目标的统计数据（如现激活人数等）
    async getGoalStatistics() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalStatistics.result);
            //}, mockDelay))
            resResultObj = mockData_1.mockCloudData.getGoalStatistics.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalStatistics`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const todoGoals = resResultObj.statistics.map((e) => {
            return new Goals_1.TodoGoal(e.goalType, e.activeNum);
        });
        return todoGoals;
    }
    // 获取本用户激活的目标的各种数据（如已完成数值、目标数值等）
    async getGoalList() {
        let resResultObj;
        if (isMock) {
            //resResultObj = await new Promise(resolve => setTimeout(() => {
            //  resolve(mockCloudData.getGoalList.result);
            //}, mockDelay));
            resResultObj = mockData_1.mockCloudData.getGoalList.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const resStr = await req.request(`${this.domain}/getGoalList}`, this.options);
            resResultObj = JSON.parse(resStr.result);
            console.log(JSON.stringify(resResultObj));
        }
        const addedGoals = resResultObj.goal.map((e) => {
            return new Goals_1.AddedGoal(e.id, e.goalType, e.completeValue, e.goalValue, e.currentWeight, e.recordDays);
        });
        return addedGoals;
    }
    // 添加新目标（除了体重目标）
    async addGoal(goalType, goalValue) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: goalType, goalValue: goalValue, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 添加体重目标
    async addWeightGoal(initWeight, targetWeight) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.addGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, goalType: Goals_1.GoalType.Weight, goalValue: targetWeight, frequency: 0, unitValue: 0 }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/addGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: addWightGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 删除目标
    async delGoal(addedGoal) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.delGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ records: [addedGoal.id] }, commonBody);
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/delGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: delGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
    // 修改编辑目标
    async updateGoal(addedGoal, goalValue, currentWeight = 0) {
        let resResultObj;
        if (isMock) {
            resResultObj = mockData_1.mockCloudData.updateGoal.result;
        }
        else {
            const req = _ohos_net_http_1.createHttp();
            const body = Object.assign({ timesType: 0, id: addedGoal.id, goalValue: goalValue, frequency: 0, unitValue: 0, currentWeight: 0 }, commonBody);
            if (addedGoal.goalType === Goals_1.GoalType.Weight) {
                body.currentWeight = currentWeight;
            }
            this.options.extraData = body;
            const resStr = await req.request(`${this.domain}/updateGoal}`, this.options);
            resResultObj = JSON.parse(resStr.result);
        }
        console.log(`CloudApis: updateGoal: ${JSON.stringify(resResultObj)}`);
        return resResultObj;
    }
}
exports["default"] = new CloudApis();


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets":
/*!********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets ***!
  \********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getDefaultDisplaySizeInVP = exports.setNextPageToastMessage = exports.showToastIfNeeded = exports.getTimeRange = exports.numberWithCommas = exports.maxDecimals = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/13
  my_annual_flag: 工具函数
 */
var _ohos_prompt_1  = globalThis.requireNapi('prompt');
var _ohos_display_1  = globalThis.requireNapi('display');
/**
 * 让数字最多保留 x 位小鼠
 * @param num 需要简化的数字
 * @param maxDecimals 最多保留的小数位数
 */
function maxDecimals(num, maxDecimals) {
    return Math.round(num * Math.pow(10, maxDecimals)) / Math.pow(10, maxDecimals);
}
exports.maxDecimals = maxDecimals;
/**
 * 给数字每千位添加一个逗号
 * @param x 数字
 * @return 添加过逗号的字符串
 */
function numberWithCommas(x) {
    return maxDecimals(x, 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
exports.numberWithCommas = numberWithCommas;
/**
 * 返回调用时当年的第一天和最后一天
 * @return 2.g. '2022/01/01-2022/12/31'
 */
function getTimeRange() {
    const date = new Date();
    const currentYear = date.getFullYear();
    const startDate = `${currentYear}/01/01`;
    const endDate = `${currentYear}12/31`;
    return `${startDate}-${endDate}`;
}
exports.getTimeRange = getTimeRange;
// 放在 onPageShow, 在页面启动时，根据是否设置 toast 字串来决定是否展示 toast
function showToastIfNeeded() {
    const msg = AppStorage.Get('toastMessage');
    if (msg) {
        _ohos_prompt_1.showToast({ message: msg });
    }
    AppStorage.SetOrCreate('toastMessage', '');
}
exports.showToastIfNeeded = showToastIfNeeded;
/**
 * 设置 toast 信息，并在下次切换页面时显示
 * @param msg 需要显示的文字
 */
function setNextPageToastMessage(msg) {
    AppStorage.SetOrCreate('toastMessage', msg);
}
exports.setNextPageToastMessage = setNextPageToastMessage;
/**
 * 获得默认显示屏的屏幕大小，vp 为单位
 * @return { width: xx, height: xx }
 */
function getDefaultDisplaySizeInVP() {
    console.log('start getDefaultDisplaySync');
    const defaultDisplay = _ohos_display_1.getDefaultDisplaySync();
    console.log('getDefaultDisplaySync' + defaultDisplay);
    return {
        width: px2vp(defaultDisplay.width),
        height: px2vp(defaultDisplay.height),
    };
    //  return {
    //    width: 360,
    //    height: 740,
    //  };
}
exports.getDefaultDisplaySizeInVP = getDefaultDisplaySizeInVP;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets":
/*!*********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/mockData.ets ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.signedDates = exports.mockCloudData = void 0;
exports.mockCloudData = {
    // 获得所有参与者的目标统计数据（坚持人数，完成人数 等...）
    getGoalStatistics: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "statistics": [
                {
                    "goalType": 0,
                    "createNum": 3801,
                    "activeNum": 1367,
                    "completeNum": 1243,
                    "delNum": 906,
                    "timeoutNum": 285
                },
                {
                    "goalType": 1,
                    "createNum": 3309,
                    "activeNum": 897,
                    "completeNum": 1241,
                    "delNum": 886,
                    "timeoutNum": 285
                },
                {
                    "goalType": 2,
                    "createNum": 1436,
                    "activeNum": 838,
                    "completeNum": 14,
                    "delNum": 143,
                    "timeoutNum": 441
                },
                {
                    "goalType": 3,
                    "createNum": 916,
                    "activeNum": 847,
                    "completeNum": 0,
                    "delNum": 67,
                    "timeoutNum": 2
                },
                {
                    "goalType": 4,
                    "createNum": 1964,
                    "activeNum": 474,
                    "completeNum": 1249,
                    "delNum": 240,
                    "timeoutNum": 1
                },
                {
                    "goalType": 5,
                    "createNum": 906,
                    "activeNum": 833,
                    "completeNum": 0,
                    "delNum": 72,
                    "timeoutNum": 1
                }
            ]
        }
    },
    getGoalList: {
        result: {
            "resultCode": 0,
            "resultDesc": "success",
            "goal": [
                {
                    "completeValue": 0,
                    "createTime": 1658217778708,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 1,
                    "goalValue": 1234,
                    "id": 23557,
                    "modifyTime": 1663845225603,
                    "recordDays": [],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 120.197,
                    "createTime": 1658217813686,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 3,
                    "goalValue": 1234,
                    "id": 23559,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                },
                {
                    "completeValue": 55.4,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 4,
                    "goalValue": 52.1,
                    "id": 23560,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 1,
                    "unitValue": 0
                },
                {
                    "completeValue": 1153834,
                    "createTime": 1658217820947,
                    "currentWeight": 0,
                    "endTime": 1672502399000,
                    "frequency": 0,
                    "goalType": 2,
                    "goalValue": 1234567,
                    "id": 23561,
                    "modifyTime": 1668138252825,
                    "recordDays": [
                        1703122137000,
                        1703001601000,
                        1699949137000,
                        1682557564000,
                        1644508800000,
                        1646755200000,
                        1649606400000,
                        1658160000000
                    ],
                    "startTime": 1640966400000,
                    "status": 0,
                    "unitValue": 0
                }
            ]
        }
    },
    addGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    delGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    },
    updateGoal: {
        result: {
            "resultCode": 0,
            "resultDesc": "success"
        }
    }
};
/**
 * 生成数个今年的随机日期
 * @param numOfDays 需要随机生成日期的数量
 * 返回随机日期数组（unix 时间戳）
 */
const generateRandomDates = function (numOfDays) {
    const currentYear = new Date().getFullYear();
    const firstDayOfYear = new Date(currentYear, 0, 1);
    const lastDayOfYear = new Date(currentYear, 11, 31);
    const res = new Set();
    for (let i = 0; i < numOfDays; i++) {
        let unique = false;
        while (!unique) {
            const randomDate = new Date(firstDayOfYear.getTime() + Math.random() * (lastDayOfYear.getTime() - firstDayOfYear.getTime())).toDateString();
            if (!res.has(randomDate)) {
                res.add(randomDate);
                unique = true;
            }
        }
    }
    return res;
};
exports.signedDates = generateRandomDates(240);


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/NoNetwork.ets":
/*!**************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/NoNetwork.ets ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.NoNetwork = void 0;
__webpack_require__(/*! @ohos.app.ability.common */ "../../api/@ohos.app.ability.common.d.ts");
class NoNetwork extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.margin({ top: 120 });
            Column.layoutWeight(1);
            Column.justifyContent(FlexAlign.Center);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 16777278, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Image.size({ width: 150, height: 150 });
            Image.objectFit(ImageFit.Contain);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create({ "id": 16777229, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Text.fontSize(16);
            Text.fontWeight(FontWeight.Medium);
            Text.margin({ top: 6 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel({ "id": 16777235, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Button.stateEffect(false);
            Button.backgroundColor(0xEFEFEF);
            Button.fontColor(0xFF841A);
            Button.fontSize(16);
            Button.margin(24);
            Button.size({ width: 250, height: 45 });
            Button.onClick(() => {
                let context = getContext(this);
                context.startAbility({
                    bundleName: 'com.ohos.settings',
                    abilityName: 'com.ohos.settings.MainAbility'
                });
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
exports.NoNetwork = NoNetwork;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets":
/*!*******************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets ***!
  \*******************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ProgressCircle = void 0;
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/17
  my_annual_flag: 应用在主页和日历页的进度环
 */
const Color_1 = __importDefault(__webpack_require__(/*! ../model/Color */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets"));
//TODO:已知模拟器默认canvas大小360*360，目前根据这个写死各种缩放逻辑，后续优化
const DEFAULT_CANVAS_SIZE = 360;
class ProgressCircle extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.slot = undefined;
        this.lineWidth = 50;
        this.bgStartAngle = 0;
        this.bgEndAngle = 2 * Math.PI;
        this.arcStartAngle = 0;
        this.arcEndAngle = 1 * Math.PI;
        this.startColor = new Color_1.default(251, 215, 134);
        this.endColor = new Color_1.default(247, 121, 125);
        this.isCounterClockWise = false;
        this.canvasSize = DEFAULT_CANVAS_SIZE;
        this.settings = new RenderingContextSettings(true);
        this.ctx = new CanvasRenderingContext2D(this.settings);
        this.drawn = false;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.slot !== undefined) {
            this.slot = params.slot;
        }
        if (params.lineWidth !== undefined) {
            this.lineWidth = params.lineWidth;
        }
        if (params.bgStartAngle !== undefined) {
            this.bgStartAngle = params.bgStartAngle;
        }
        if (params.bgEndAngle !== undefined) {
            this.bgEndAngle = params.bgEndAngle;
        }
        if (params.arcStartAngle !== undefined) {
            this.arcStartAngle = params.arcStartAngle;
        }
        if (params.arcEndAngle !== undefined) {
            this.arcEndAngle = params.arcEndAngle;
        }
        if (params.startColor !== undefined) {
            this.startColor = params.startColor;
        }
        if (params.endColor !== undefined) {
            this.endColor = params.endColor;
        }
        if (params.isCounterClockWise !== undefined) {
            this.isCounterClockWise = params.isCounterClockWise;
        }
        if (params.canvasSize !== undefined) {
            this.canvasSize = params.canvasSize;
        }
        if (params.settings !== undefined) {
            this.settings = params.settings;
        }
        if (params.ctx !== undefined) {
            this.ctx = params.ctx;
        }
        if (params.drawn !== undefined) {
            this.drawn = params.drawn;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    /**
     * 计算渐变颜色
     * @param start 初始颜色
     * @param end 结束颜色
     * @param step 渐变段数（段数越高，渐变越平滑）
     * @return 颜色数组（长度等于step）
     */
    gradientColor(start, end, step) {
        const arr = [];
        const diffR = (end.r - start.r) / step;
        const diffG = (end.g - start.g) / step;
        const diffB = (end.b - start.b) / step;
        for (let i = 0; i < step; i++) {
            const color = new Color_1.default(diffR * i + start.r, diffG * i + start.g, diffB * i + start.b);
            arr.push(color);
        }
        return arr;
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.Center });
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Canvas.create(this.ctx);
            Canvas.size({ width: this.canvasSize, height: this.canvasSize });
            Canvas.onReady(() => {
                if (this.drawn)
                    return;
                //圆环背景
                this.ctx.beginPath();
                const s = this.canvasSize / DEFAULT_CANVAS_SIZE;
                this.ctx.translate(DEFAULT_CANVAS_SIZE / 2 * s, DEFAULT_CANVAS_SIZE / 2 * s);
                this.ctx.scale(s, s);
                this.ctx.lineWidth = this.lineWidth;
                this.ctx.strokeStyle = 'rgba(0,0,0,0.03)';
                this.ctx.lineCap = 'round';
                const radius = DEFAULT_CANVAS_SIZE / 2 - this.lineWidth / 2;
                this.ctx.arc(0, 0, radius, this.bgStartAngle, this.bgEndAngle, this.isCounterClockWise);
                this.ctx.stroke();
                this.ctx.closePath();
                const unit = 0.05;
                const progress = (this.arcStartAngle - this.arcStartAngle)
                    / (this.bgEndAngle - this.bgStartAngle);
                if (progress >= 0.01 && progress < 0.02) {
                    this.ctx.beginPath();
                    this.ctx.arc(0, 0, radius, Math.PI * 3 / 2, Math.PI * 3 / 2, false);
                    this.ctx.strokeStyle = this.startColor.toHex();
                    this.ctx.lineCap = 'round';
                    this.ctx.stroke();
                    this.ctx.closePath();
                    return;
                }
                const division = Math.floor((this.arcEndAngle - this.arcStartAngle) / unit);
                const gradient = this.gradientColor(this.startColor, this.endColor, division);
                let start = this.arcStartAngle;
                let end = start;
                for (let i = 0; i < division; i++) {
                    this.ctx.beginPath();
                    this.ctx.lineCap = 'round';
                    end = start + unit;
                    this.ctx.lineWidth = this.lineWidth;
                    this.ctx.strokeStyle = gradient[i].toRGB();
                    this.ctx.arc(0, 0, radius, start, end);
                    this.ctx.stroke();
                    start += unit;
                }
                this.drawn = true;
            });
            if (!isInitialRender) {
                Canvas.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Canvas.pop();
        this.slot.bind(this)();
        Stack.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
exports.ProgressCircle = ProgressCircle;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/17
  my_annual_flag: 颜色对象
 */
class Color {
    constructor(r, g, b) {
        this.r = 0;
        this.g = 0;
        this.b = 0;
        this.r = Math.round(r);
        this.g = Math.round(g);
        this.b = Math.round(b);
    }
    toHex() {
        return `#${this.num2Hex(this.r)}${this.num2Hex(this.g)}${this.num2Hex(this.b)}`;
    }
    toRGB() {
        return `rgb(${this.r}, ${this.g}, ${this.b})`;
    }
    num2Hex(n) {
        const hex = n.toString(16);
        return hex.length == 1 ? `0${hex}}` : hex;
    }
}
exports["default"] = Color;


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets":
/*!*****************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets ***!
  \*****************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AddedGoal = exports.TodoGoal = exports.GoalType = void 0;
const Color_1 = __importDefault(__webpack_require__(/*! ./Color */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Color.ets"));
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
var GoalType;
(function (GoalType) {
    GoalType[GoalType["Running"] = 0] = "Running";
    GoalType[GoalType["Fitness"] = 1] = "Fitness";
    GoalType[GoalType["Walking"] = 2] = "Walking";
    GoalType[GoalType["Cycling"] = 3] = "Cycling";
    GoalType[GoalType["Weight"] = 4] = "Weight";
    GoalType[GoalType["Swimming"] = 5] = "Swimming";
})(GoalType = exports.GoalType || (exports.GoalType = {}));
const goalsResourceMap = [
    {
        name: { "id": 16777224, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777291, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777255, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777246, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777283, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起奔跑，最美的风景是下一公里！`,
        unit: '公里'
    },
    {
        name: { "id": 16777223, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777288, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777252, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777245, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777282, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，享受流汗的快乐！`,
        unit: '分钟'
    },
    {
        name: { "id": 16777226, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(255, 163, 51), new Color_1.default(255, 133, 25)],
        imgSrc: { "id": 16777276, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777253, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777244, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777286, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起向前，走向广阔世界！`,
        unit: '步'
    },
    {
        name: { "id": 16777222, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(242, 88, 135), new Color_1.default(224, 45, 80)],
        imgSrc: { "id": 16777292, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777254, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777247, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777280, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起骑行，战胜风，也享受风！`,
        unit: '公里'
    },
    {
        name: { "id": 16777227, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777293, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777274, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777248, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777281, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，将自律进行到底！`,
        unit: '公斤'
    },
    {
        name: { "id": 16777225, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        color: [new Color_1.default(134, 193, 255), new Color_1.default(37, 79, 247)],
        imgSrc: { "id": 16777295, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        iconSrc: { "id": 16777256, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        addImgSrc: { "id": 16777249, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        shareImgSrc: { "id": 16777284, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" },
        slogan: (n) => `与 ${n} 人一起坚持，水的阻力，不及你的毅力！`,
        unit: '公里'
    }
];
class TodoGoal {
    constructor(goalType, activeNum) {
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.image = goalsResourceMap[this.goalType].imgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.addImage = goalsResourceMap[this.goalType].addImgSrc;
        this.activeNum = activeNum;
        this.slogan = goalsResourceMap[this.goalType].slogan(helpers_1.numberWithCommas(activeNum));
    }
}
exports.TodoGoal = TodoGoal;
// 已被用户添加并且跟踪目标
class AddedGoal {
    constructor(id, goalType, completeValue, goalValue, initWeight, recordDaysTimeStamps) {
        this.id = id;
        this.goalType = goalType;
        this.goalName = goalsResourceMap[this.goalType].name;
        this.icon = goalsResourceMap[this.goalType].iconSrc;
        this.shareImg = goalsResourceMap[this.goalType].shareImgSrc;
        this.unit = goalsResourceMap[this.goalType].unit;
        this.completeValue = completeValue;
        this.goalValue = goalValue;
        this.color = goalsResourceMap[this.goalType].color;
        this.initWeight = initWeight;
        this.isWeight = this.goalType === 4;
        this.recordDaysTimeStamps = recordDaysTimeStamps;
        this.progress = this.isWeight ? this.completeValue / this.goalValue : this.completeValue / this.goalValue;
        this.progress = this.isWeight
            ? computeWeightProgress(this.initWeight, this.completeValue, this.goalValue)
            : computeProgress(0, this.completeValue, this.goalValue);
    }
}
exports.AddedGoal = AddedGoal;
// 计算目标完成进度
function computeProgress(start, curr, target) {
    const res = (curr - start) / (target - start);
    if (res < 0)
        return 0;
    if (res >= 0.99 && res < 0.9995)
        return 0.99;
    if (res >= 0.9995)
        return 1;
    return res;
}
// 计算体重目标完成进度
function computeWeightProgress(start, curr, target) {
    if (curr === 0) {
        return 0;
    }
    if (target > start) {
        if (curr <= start) {
            return 0;
        }
        if (curr >= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else if (target < start) {
        if (curr <= start) {
            return 0;
        }
        if (curr <= target) {
            return 1;
        }
        return computeProgress(start, curr, target);
    }
    else {
        return 1;
    }
}


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets":
/*!**********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/ThemeConst.ets ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DefaultTheme = void 0;
//所有能配置主题的相关资源
//// BACKGROUND_CARD 卡片背景颜色
//// BACKGROUND_MAIN 主页背景色
//// FONT_COLOR_MAIN 字体背景颜色
////CARD_PANEL_BACKGROUND panel弹窗背景颜色（日历，设定目标）
//// START_WINDOW_BACKGROUND ??
//// IC_BACK 返回图标
//// IC_PUBLIC_DEL 删除图标
//// IC_PUBLIC_DET 告警（详情）图标
//// IC_PUBLIC_EDIT 修改图标
//// IC_PUBLIC_HIS 历史图标
//// IC_PUBLIC_MORE 修改删除图标
//// IC_PUBLIC_SHARE 分享图标
//// 系统主题 包括 深浅模式 通过dark，light目录实现
class DefaultTheme {
}
exports.DefaultTheme = DefaultTheme;
DefaultTheme.BACKGROUND_CARD = { "id": 16777238, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BACKGROUND_MAIN = { "id": 16777239, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.FONT_COLOR_MAIN = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.START_WINDOW_BACKGROUND = { "id": 16777241, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.CARD_PANEL_BACKGROUND = { "id": 16777237, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.BIG_BTN_BACKGROUND_COLOR = { "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_BACK = { "id": 16777257, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DEL = { "id": 16777262, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_DET = { "id": 16777264, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_EDIT = { "id": 16777266, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_HIS = { "id": 16777268, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_MORE = { "id": 16777270, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };
DefaultTheme.IC_PUBLIC_SHARE = { "id": 16777299, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" };


/***/ }),

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/Index.ets?entry":
/*!***********************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/Index.ets?entry ***!
  \***********************************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Create: 2022/9/23
  my_annual_flag: 年度目标主页
*/
var _ohos_router_1  = globalThis.requireNapi('router');
__webpack_require__(/*! ../model/Goals */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/model/Goals.ets");
const helpers_1 = __webpack_require__(/*! ../common/helpers */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/helpers.ets");
const NoNetwork_1 = __webpack_require__(/*! ../components/NoNetwork */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/NoNetwork.ets");
const ProgressCircle_1 = __webpack_require__(/*! ../components/ProgressCircle */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/components/ProgressCircle.ets");
const cloudApis_1 = __importDefault(__webpack_require__(/*! ../common/cloudApis */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/cloudApis.ets"));
var _ohos_promptAction_1  = globalThis.requireNapi('promptAction');
const ThemeGet_1 = __webpack_require__(/*! ../common/ThemeGet */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ThemeGet.ets");
const Channel_1 = __importDefault(__webpack_require__(/*! ../common/Channel */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Channel.ets"));
//测试接口用
const Logger_1 = __importDefault(__webpack_require__(/*! ../common/Logger */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/Logger.ets"));
var _ohos_net_http_1  = globalThis.requireNapi('net.http');
const ComponentSnapshot_1 = __importDefault(__webpack_require__(/*! ../common/ComponentSnapshot */ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/common/ComponentSnapshot.ets"));
var _ohos_deviceInfo_1  = globalThis.requireNapi('deviceInfo');
var _ohos_intl_1  = globalThis.requireNapi('intl');
var _ohos_i18n_1  = globalThis.requireNapi('i18n');
var _ohos_mediaquery_1  = globalThis.requireNapi('mediaquery');
var _ohos_url_1  = globalThis.requireNapi('url');
var _ohos_net_connection_1  = globalThis.requireNapi('net.connection');
class Index extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__displaySize = new ObservedPropertySimplePU(SizeType.XS, this, "displaySize");
        this.__todoGoals = new ObservedPropertyObjectPU([], this, "todoGoals");
        this.__addedGoals = new ObservedPropertyObjectPU([], this, "addedGoals");
        this.__isLoading = new ObservedPropertySimplePU(true, this, "isLoading");
        this.__currYear = new ObservedPropertySimplePU(1900, this, "currYear");
        this.__isConnectNetwork = this.createStorageLink('isConnectNetwork', true, "isConnectNetwork");
        this.__theme = this.createStorageLink('theme', -1, "theme");
        this.__useBridge = new ObservedPropertySimplePU(true, this, "useBridge");
        this.__channel = new ObservedPropertyObjectPU(null, this, "channel");
        this.__tag = new ObservedPropertySimplePU('Interface test tag', this, "tag");
        this.netConnection = _ohos_net_connection_1.createNetConnection();
        this.setInitiallyProvidedValue(params);
        this.declareWatch("isConnectNetwork", this.netWorkChange);
    }
    setInitiallyProvidedValue(params) {
        if (params.displaySize !== undefined) {
            this.displaySize = params.displaySize;
        }
        if (params.todoGoals !== undefined) {
            this.todoGoals = params.todoGoals;
        }
        if (params.addedGoals !== undefined) {
            this.addedGoals = params.addedGoals;
        }
        if (params.isLoading !== undefined) {
            this.isLoading = params.isLoading;
        }
        if (params.currYear !== undefined) {
            this.currYear = params.currYear;
        }
        if (params.useBridge !== undefined) {
            this.useBridge = params.useBridge;
        }
        if (params.channel !== undefined) {
            this.channel = params.channel;
        }
        if (params.tag !== undefined) {
            this.tag = params.tag;
        }
        if (params.netConnection !== undefined) {
            this.netConnection = params.netConnection;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__displaySize.purgeDependencyOnElmtId(rmElmtId);
        this.__todoGoals.purgeDependencyOnElmtId(rmElmtId);
        this.__addedGoals.purgeDependencyOnElmtId(rmElmtId);
        this.__isLoading.purgeDependencyOnElmtId(rmElmtId);
        this.__currYear.purgeDependencyOnElmtId(rmElmtId);
        this.__useBridge.purgeDependencyOnElmtId(rmElmtId);
        this.__channel.purgeDependencyOnElmtId(rmElmtId);
        this.__tag.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__displaySize.aboutToBeDeleted();
        this.__todoGoals.aboutToBeDeleted();
        this.__addedGoals.aboutToBeDeleted();
        this.__isLoading.aboutToBeDeleted();
        this.__currYear.aboutToBeDeleted();
        this.__isConnectNetwork.aboutToBeDeleted();
        this.__theme.aboutToBeDeleted();
        this.__useBridge.aboutToBeDeleted();
        this.__channel.aboutToBeDeleted();
        this.__tag.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get displaySize() {
        return this.__displaySize.get();
    }
    set displaySize(newValue) {
        this.__displaySize.set(newValue);
    }
    get todoGoals() {
        return this.__todoGoals.get();
    }
    set todoGoals(newValue) {
        this.__todoGoals.set(newValue);
    }
    get addedGoals() {
        return this.__addedGoals.get();
    }
    set addedGoals(newValue) {
        this.__addedGoals.set(newValue);
    }
    get isLoading() {
        return this.__isLoading.get();
    }
    set isLoading(newValue) {
        this.__isLoading.set(newValue);
    }
    get currYear() {
        return this.__currYear.get();
    }
    set currYear(newValue) {
        this.__currYear.set(newValue);
    }
    get isConnectNetwork() {
        return this.__isConnectNetwork.get();
    }
    set isConnectNetwork(newValue) {
        this.__isConnectNetwork.set(newValue);
    }
    get theme() {
        return this.__theme.get();
    }
    set theme(newValue) {
        this.__theme.set(newValue);
    }
    get useBridge() {
        return this.__useBridge.get();
    }
    set useBridge(newValue) {
        this.__useBridge.set(newValue);
    }
    get channel() {
        return this.__channel.get();
    }
    set channel(newValue) {
        this.__channel.set(newValue);
    }
    get tag() {
        return this.__tag.get();
    }
    set tag(newValue) {
        this.__tag.set(newValue);
    }
    aboutToAppear() {
        this.initData();
    }
    netWorkChange() {
        this.initData();
    }
    initData() {
        if (!this.isConnectNetwork) {
            this.isLoading = false;
        }
        else {
            this.isLoading = true;
            helpers_1.showToastIfNeeded();
            this.currYear = new Date().getFullYear();
            this.initGoals();
            const message = AppStorage.Get('toastMessage');
        }
        if (this.useBridge) {
            this.channel = new Channel_1.default('Bridge');
        }
        this.urlTest();
        this.httpTest();
        this.interfaceTest();
    }
    urlTest() {
        Logger_1.default.info(this.tag, 'start urlTest');
        let objectParams = new _ohos_url_1.URLParams([['user1', 'abc1'], ['query2', 'first2'], ['query3', 'second3']]);
        let objectParams1 = new _ohos_url_1.URLParams({ "fod": '1', "bard": '2' });
        let objectParams2 = new _ohos_url_1.URLParams('?fod=1&bard=2');
        let urlObject = _ohos_url_1.URL.parseURL('https://developer.mozilla.org/?fod=1&bard=2');
        let params = new _ohos_url_1.URLParams(urlObject.search);
        Logger_1.default.info(this.tag, `urlObject=${urlObject},params=${params}`);
        urlObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        let paramsObject = new _ohos_url_1.URLParams(urlObject.search.slice(1));
        paramsObject.append('fod', '3');
        Logger_1.default.info(this.tag, `urlObject=${urlObject},paramsObject=${paramsObject}`);
        urlObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        paramsObject = new _ohos_url_1.URLParams(urlObject.search.slice(1));
        paramsObject.delete('fod');
        Logger_1.default.info(this.tag, `urlObject=${urlObject},paramsObject=${paramsObject}`);
        urlObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        params = new _ohos_url_1.URLParams(urlObject.search.slice(1));
        params.append('fod', '3'); // Add a second value for the fod parameter.
        console.log(params.getAll('fod').toString()); // Output ["1","3"].
        Logger_1.default.info(this.tag, `getAll=${params.getAll('fod').toString()}`);
        let searchParamsObject = new _ohos_url_1.URLParams("keyName1=valueName1&keyName2=valueName2");
        for (var pair of searchParamsObject.entries()) { // Show keyName/valueName pairs
            Logger_1.default.info(this.tag, `pair[0]=${pair[0]},pair[1]=${pair[1]}`);
        }
        const myURLObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        myURLObject.params.forEach((value, name, searchParams) => {
            Logger_1.default.info(this.tag, `name=${name},value=${value},myURLObject.params === searchParams=${myURLObject.params === searchParams}`);
        });
        paramsObject = new _ohos_url_1.URLParams('name=Jonathan&age=18');
        let name = paramsObject.get("name"); // is the string "Jonathan"
        let age = parseInt(paramsObject.get("age"), 10); // is the number 18
        Logger_1.default.info(this.tag, `name=${name},age=${age}`);
        urlObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        paramsObject = new _ohos_url_1.URLParams(urlObject.search.slice(1));
        let result = paramsObject.has('bard');
        Logger_1.default.info(this.tag, `result=${result}`);
        urlObject = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        Logger_1.default.info(this.tag, `urlObject=${urlObject}`);
        paramsObject = new _ohos_url_1.URLParams(urlObject.search.slice(1));
        paramsObject.set('baz', '3'); // Add a third parameter.
        Logger_1.default.info(this.tag, `urlObject=${urlObject}`);
        searchParamsObject = new _ohos_url_1.URLParams("c=3&a=9&b=4&d=2"); // Create a test URLSearchParams object
        searchParamsObject.sort(); // Sort the key/value pairs
        Logger_1.default.info(this.tag, `searchParamsObject.toString()=${searchParamsObject.toString()}`); // Display the sorted query string // Output a=9&b=2&c=3&d=4
        searchParamsObject = new _ohos_url_1.URLParams("key1=value1&key2=value2"); // Create a URLSearchParamsObject object for testing
        for (var key of searchParamsObject.keys()) { // Output key-value pairs
            Logger_1.default.info(this.tag, `key=${key}`);
        }
        let searchParams = new _ohos_url_1.URLParams("key1=value1&key2=value2"); // Create a URLSearchParamsObject object for testing
        for (var value of searchParams.values()) {
            Logger_1.default.info(this.tag, `value=${value}`);
        }
        paramsObject = new _ohos_url_1.URLParams('fod=bay&edg=bap');
        for (const [name, value] of paramsObject[Symbol.iterator]()) {
            Logger_1.default.info(this.tag, `name=${name},value=${value}`);
        }
        let url = _ohos_url_1.URL.parseURL('https://developer.exampleUrl/?fod=1&bard=2');
        params = new _ohos_url_1.URLParams(url.search.slice(1));
        params.append('fod', '3');
        Logger_1.default.info(this.tag, `params.toString()=${params.toString()}`);
        let mm = 'https://username:password@host:8080';
        Logger_1.default.info(this.tag, `mm=${mm}`);
        let a = new _ohos_url_1.URL("/", mm); // Output 'https://username:password@host:8080/';
        Logger_1.default.info(this.tag, `a=${a}`);
        let b = new _ohos_url_1.URL(mm); // Output 'https://username:password@host:8080/';
        Logger_1.default.info(this.tag, `b=${b}`);
        Logger_1.default.info(this.tag, `new Url.URL('path/path1', b)=${new _ohos_url_1.URL('path/path1', b)}`); // Output 'https://username:password@host:8080/path/path1';
        let c = new _ohos_url_1.URL('/path/path1', b); // Output 'https://username:password@host:8080/path/path1';
        Logger_1.default.info(this.tag, `c=${c}`);
        Logger_1.default.info(this.tag, `new Url.URL('/path/path1', c)=${new _ohos_url_1.URL('/path/path1', c)}`); // Output 'https://username:password@host:8080/path/path1';
        Logger_1.default.info(this.tag, `new Url.URL('/path/path1', a)=${new _ohos_url_1.URL('/path/path1', a)}`); // Output 'https://username:password@host:8080/path/path1';
        let d = new _ohos_url_1.URL('/path/path1', "https://www.exampleUrl/fr-FR/toto"); // Output https://www.exampleUrl/path/path1
        d = new _ohos_url_1.URL('/path/path1', ''); // Raises a TypeError exception as '' is not a valid URL
        Logger_1.default.info(this.tag, `d=${d}`);
        d = new _ohos_url_1.URL('/path/path1'); // Raises a TypeError exception as '/path/path1' is not a valid URL
        Logger_1.default.info(this.tag, `d=${d}`);
        d = new _ohos_url_1.URL('https://www.example.com'); // Output https://www.example.com/
        Logger_1.default.info(this.tag, `d=${d}`);
        d = new _ohos_url_1.URL('https://www.example.com', b); // Output https://www.example.com/
        Logger_1.default.info(this.tag, `d=${d}`);
        mm = 'https://username:password@host:8080';
        url = _ohos_url_1.URL.parseURL(mm);
        let resultB = url.toString(); // Output 'https://username:password@host:8080/'
        Logger_1.default.info(this.tag, `resultB=${resultB}`);
        url = _ohos_url_1.URL.parseURL('https://username:password@host:8080/directory/file?query=pppppp#qwer=da');
        let resultS = url.toString();
        Logger_1.default.info(this.tag, `resultS=${resultS}`);
        url = _ohos_url_1.URL.parseURL('https://username:password@host:8080/directory/file?query=pppppp#qwer=da');
        let resultJ = url.toJSON();
        Logger_1.default.info(this.tag, `resultJ=${resultJ}`);
        Logger_1.default.info(this.tag, 'end urlTest');
    }
    onPortrait(mediaQueryResult) {
        Logger_1.default.info(this.tag, 'start onPortrait');
        let color = '';
        let text = '';
        if (mediaQueryResult.matches) {
            color = '#FFD700';
            text = 'Landscape';
        }
        else {
            color = '#DB7093';
            text = 'Portrait';
        }
        Logger_1.default.info(this.tag, `color=${color},text=${text}`);
        Logger_1.default.info(this.tag, 'end onPortrait');
    }
    intlTest() {
        Logger_1.default.info(this.tag, 'start intlTest');
        // 默认构造函数使用系统当前locale创建Locale对象
        let locale = new _ohos_intl_1.Locale();
        // 返回系统当前localel
        let localeID = locale.toString();
        Logger_1.default.info(this.tag, `localeID=${localeID}`);
        // 创建 "zh-CN" Locale对象
        locale = new _ohos_intl_1.Locale("zh-CN");
        localeID = locale.toString(); // localeID = "zh-CN"
        Logger_1.default.info(this.tag, `localeID=${localeID}`);
        locale = new _ohos_intl_1.Locale("en-GB");
        localeID = locale.toString();
        Logger_1.default.info(this.tag, `localeID=${localeID}`);
        // 创建 "zh" Locale对象
        locale = new _ohos_intl_1.Locale("zh");
        // 补齐Locale对象的脚本和地区
        let maximizedLocale = locale.maximize();
        localeID = maximizedLocale.toString(); // localeID = "zh-Hans-CN"
        Logger_1.default.info(this.tag, `localeID=${localeID}`);
        // 创建 "zh-Hans-CN" Locale对象
        locale = new _ohos_intl_1.Locale("zh-Hans-CN");
        // 去除Locale对象的脚本和地区
        let minimizedLocale = locale.minimize();
        localeID = minimizedLocale.toString(); // localeID = "zh"
        Logger_1.default.info(this.tag, `localeID=${localeID}`);
        let datefmt = new _ohos_intl_1.DateTimeFormat();
        Logger_1.default.info(this.tag, `datefmt=${datefmt}`);
        let date = new Date(2021, 11, 17, 3, 24, 0);
        // 使用 en-GB locale创建DateTimeFormat对象
        datefmt = new _ohos_intl_1.DateTimeFormat("en-GB");
        let formattedDate = datefmt.format(date); // formattedDate "17/12/2021"
        Logger_1.default.info(this.tag, `formattedDate=${formattedDate}`);
        // 使用 en-GB locale创建DateTimeFormat对象，dateStyle设置为full，timeStyle设置为medium
        datefmt = new _ohos_intl_1.DateTimeFormat("en-GB", { dateStyle: 'full', timeStyle: 'medium' });
        formattedDate = datefmt.format(date); // formattedDate "Friday, 17 December 2021 at 03:24:00"
        Logger_1.default.info(this.tag, `formattedDate=${formattedDate}`);
        let startDate = new Date(2021, 11, 17, 3, 24, 0);
        let endDate = new Date(2021, 11, 18, 3, 24, 0);
        // 使用 en-GB locale创建DateTimeFormat对象
        datefmt = new _ohos_intl_1.DateTimeFormat("en-GB");
        let formattedDateRange = datefmt.formatRange(startDate, endDate); // formattedDateRange = "17/12/2021-18/12/2021"
        Logger_1.default.info(this.tag, `formattedDateRange=${formattedDateRange}`);
        datefmt = new _ohos_intl_1.DateTimeFormat("en-GB", { dateStyle: 'full', timeStyle: 'medium' });
        // 返回DateTimeFormat对象的配置项
        let options = datefmt.resolvedOptions();
        let dateStyle = options.dateStyle; // dateStyle = "full"
        let timeStyle = options.timeStyle; // timeStyle = "medium"
        Logger_1.default.info(this.tag, `dateStyle=${dateStyle},timeStyle=${timeStyle}`);
        // 使用 en-GB locale创建NumberFormat对象，style设置为decimal，notation设置为scientific
        let numfmt = new _ohos_intl_1.NumberFormat("en-GB", { style: 'decimal', notation: "scientific" });
        Logger_1.default.info(this.tag, `numfmt=${numfmt}`);
        let formattedNumber = numfmt.format(1223); // formattedNumber = 1.223E3
        Logger_1.default.info(this.tag, `formattedNumber=${formattedNumber}`);
        numfmt = new _ohos_intl_1.NumberFormat(["en-GB", "zh"], { style: 'decimal', notation: "scientific" });
        // 获取NumberFormat对象配置项
        let options1 = numfmt.resolvedOptions();
        let style = options1.style; // style = decimal
        let notation = options1.notation; // notation = scientific
        Logger_1.default.info(this.tag, `style=${style},notation=${notation}`);
        // 使用en-GB locale创建Collator对象
        let collator = new _ohos_intl_1.Collator("en-GB");
        // 比较 "first" 和 "second" 的先后顺序
        let compareResult = collator.compare("first", "second"); // compareResult = -1
        Logger_1.default.info(this.tag, `compareResult=${compareResult}`);
        // 使用 en-US locale创建PluralRules对象
        let enPluralRules = new _ohos_intl_1.PluralRules("en-US");
        // 计算 en-US locale中数字1对应的单复数类别
        let plural = enPluralRules.select(1); // plural = one
        Logger_1.default.info(this.tag, `plural=${plural}`);
        // 使用 zh-CN locale创建RelativeTimeFormat对象
        let relativetimefmt = new _ohos_intl_1.RelativeTimeFormat("zh-CN");
        // 计算 zh-CN locale中数字3，单位quarter的本地化表示
        let formatResult = relativetimefmt.format(3, "quarter"); // formatResult = "3个季度后"
        Logger_1.default.info(this.tag, `formatResult=${formatResult}`);
        // 使用 en locale创建RelativeTimeFormat对象，numeric设置为auto
        relativetimefmt = new _ohos_intl_1.RelativeTimeFormat("en", { "numeric": "auto" });
        let parts = relativetimefmt.formatToParts(10, "seconds"); // parts = [ {type: "literal", value: "in"}, {type: "integer", value: 10, unit: "second"}, {type: "literal", value: "seconds"} ]
        Logger_1.default.info(this.tag, `parts=${parts}`);
    }
    i18nTest() {
        Logger_1.default.info(this.tag, 'start i18nTest');
        try {
            let displayCountry = _ohos_i18n_1.System.getDisplayCountry("zh-CN", "en-GB"); // displayCountry = "China"
            Logger_1.default.info(this.tag, `displayCountry=${displayCountry}`);
        }
        catch (error) {
            console.error(`call System.getDisplayCountry failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let displayLanguage = _ohos_i18n_1.System.getDisplayLanguage("zh", "en-GB"); // displayLanguage = Chinese
            Logger_1.default.info(this.tag, `displayLanguage=${displayLanguage}`);
        }
        catch (error) {
            console.error(`call System.getDisplayLanguage failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let systemLanguages = _ohos_i18n_1.System.getSystemLanguages(); // [ "en-Latn-US", "zh-Hans" ]
            Logger_1.default.info(this.tag, `systemLanguages=${systemLanguages}`);
        }
        catch (error) {
            console.error(`call System.getSystemLanguages failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let systemCountries = _ohos_i18n_1.System.getSystemCountries('zh'); // systemCountries = [ "ZW", "YT", "YE", ..., "ER", "CN", "DE" ]，共计240个国家或地区
            Logger_1.default.info(this.tag, `systemCountries=${systemCountries}`);
        }
        catch (error) {
            console.error(`call System.getSystemCountries failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let res = _ohos_i18n_1.System.isSuggested('zh', 'CN'); // res = true
            Logger_1.default.info(this.tag, `res=${res}`);
        }
        catch (error) {
            console.error(`call System.isSuggested failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let systemLanguage = _ohos_i18n_1.System.getSystemLanguage(); // systemLanguage为当前系统语言
            Logger_1.default.info(this.tag, `systemLanguage=${systemLanguage}`);
        }
        catch (error) {
            console.error(`call System.getSystemLanguage failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let systemRegion = _ohos_i18n_1.System.getSystemRegion(); // 获取系统当前地区设置
            Logger_1.default.info(this.tag, `systemRegion=${systemRegion}`);
        }
        catch (error) {
            console.error(`call System.getSystemRegion failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let systemLocale = _ohos_i18n_1.System.getSystemLocale(); // 获取系统当前Locale
            Logger_1.default.info(this.tag, `systemLocale=${systemLocale}`);
        }
        catch (error) {
            console.error(`call System.getSystemLocale failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let is24HourClock = _ohos_i18n_1.System.is24HourClock(); // 系统24小时开关是否开启
            Logger_1.default.info(this.tag, `is24HourClock=${is24HourClock}`);
        }
        catch (error) {
            console.error(`call System.is24HourClock failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let preferredLanguageList = _ohos_i18n_1.System.getPreferredLanguageList(); // 获取系统当前偏好语言列表
            Logger_1.default.info(this.tag, `preferredLanguageList=${preferredLanguageList}`);
        }
        catch (error) {
            console.error(`call System.getPreferredLanguageList failed, error code: ${error.code}, message: ${error.message}.`);
        }
        try {
            let status = _ohos_i18n_1.System.getUsingLocalDigit(); // 判断本地化数字开关是否打开
            Logger_1.default.info(this.tag, `status=${status}`);
        }
        catch (error) {
            console.error(`call System.getUsingLocalDigit failed, error code: ${error.code}, message: ${error.message}.`);
        }
        Logger_1.default.info(this.tag, `I18n.isRTL("zh-CN")=${_ohos_i18n_1.isRTL("zh-CN")}`); // 中文不是RTL语言，返回false
        Logger_1.default.info(this.tag, `I18n.isRTL("ar-EG")=${_ohos_i18n_1.isRTL("ar-EG")}`); // 阿语是RTL语言，返回true
        let calendar = _ohos_i18n_1.getCalendar("zh-Hans", "chinese");
        let firstDayOfWeek = calendar.getFirstDayOfWeek();
        Logger_1.default.info(this.tag, `firstDayOfWeek=${firstDayOfWeek}`);
        Logger_1.default.info(this.tag, `calendar,year=${calendar.get('year')},month=${calendar.get('month')},hour=${calendar.get('hour')}`);
        calendar.set(2021, 10, 1, 8, 0, 0);
        Logger_1.default.info(this.tag, `calendar,year=${calendar.get('year')},month=${calendar.get('month')},hour=${calendar.get('hour')}`);
        let calendarName = calendar.getDisplayName("zh"); // calendarName = "佛历"
        Logger_1.default.info(this.tag, `calendarName=${calendarName}`);
        Logger_1.default.info(this.tag, 'end i18nTest');
        Logger_1.default.info(this.tag, 'start connectionTest');
        _ohos_net_connection_1.hasDefaultNet((error, has) => {
            console.log("DefaultNet: " + JSON.stringify(error));
            if (has == undefined) {
                Logger_1.default.info(this.tag, `connectionTest = ${error.message}`);
                return;
            }
            console.log("DefaultNet: " + has);
        });
        Logger_1.default.info(this.tag, 'end connectionTest');
    }
    interfaceTest() {
        Logger_1.default.info(this.tag, 'start interfaceTest');
        Logger_1.default.info(this.tag, 'start mediaQueryResult');
        let listener = _ohos_mediaquery_1.matchMediaSync('(orientation: landscape)');
        let portraitFunc = this.onPortrait.bind(this); // bind current js instance
        listener.on('change', portraitFunc);
        Logger_1.default.info(this.tag, 'end mediaQueryResult');
        Logger_1.default.info(this.tag, `deviceInfo , brand=${_ohos_deviceInfo_1.brand},manufacture=${_ohos_deviceInfo_1.manufacture},productModel=${_ohos_deviceInfo_1.productModel}`);
        console.log(`setTimeOut start`);
        setTimeout(() => {
            console.log(`setTimeOut execute`);
            Logger_1.default.info(this.tag, 'start ComponentSnapshot');
            let snap = new ComponentSnapshot_1.default();
            snap.getBase64('test_snapshot').then((base64Str) => {
                Logger_1.default.info(this.tag, `base64Str.length =${base64Str.length} , ${base64Str.toString()}}`);
                let array = new Array(base64Str.length);
                for (let i = 0; i < base64Str.length; i++) {
                    array[i] = base64Str[i];
                }
                this.channel.sendMessage(array);
            });
            Logger_1.default.info(this.tag, 'end ComponentSnapshot');
        }, 10000);
        Logger_1.default.info(this.tag, 'end interfaceTest');
    }
    async httpTest() {
        Logger_1.default.info(this.tag, 'start httpTest');
        const req = _ohos_net_http_1.createHttp();
        req.request(`https://www.baidu.com/`, (err, data) => {
            if (!err) {
                Logger_1.default.info(this.tag, `Result=${data.result}`);
                console.info(this.tag + 'code:' + data.responseCode);
                console.info(this.tag + 'header:' + JSON.stringify(data.header));
                console.info(this.tag + 'cookies:' + data.cookies); // 8+
            }
            else {
                Logger_1.default.info(this.tag, `error=${JSON.stringify(err)}`);
            }
        });
        req.request2("https://www.baidu.com/", (err, data) => {
            if (!err) {
                Logger_1.default.info(this.tag, `request2 OK! ResponseCode is=${JSON.stringify(data)}`);
            }
            else {
                Logger_1.default.info(this.tag, `request2 ERROR! err is=${JSON.stringify(err)}`);
            }
        });
        setTimeout(() => {
            Logger_1.default.info(this.tag, 'start httpTest destroy');
            const resultDestroy = req.destroy();
            Logger_1.default.info(this.tag, 'end httpTest destroy');
        }, 10000);
        Logger_1.default.info(this.tag, 'end httpTest');
    }
    aboutToDisappear() {
        //测试时暂时修改为false，正常应该设置为true
        this.isLoading = true;
    }
    //初始化，云侧获取目标数据
    async initGoals() {
        const requests = await Promise.all([cloudApis_1.default.getGoalStatistics(), cloudApis_1.default.getGoalList()]);
        this.isLoading = false;
        this.todoGoals = requests[0];
        this.addedGoals = requests[1];
    }
    /**
     * 根据goalType从goalStatistics中获取激活目标的用户人数
     * @param goalType
     * @return 激活目标的用户人数
     */
    getActiveNum(goalType) {
        let activeNum = -1;
        this.todoGoals.forEach((goal) => {
            if (goal.goalType === goalType) {
                activeNum = goal.activeNum;
            }
        });
        return activeNum;
    }
    //导航栏标题（因为没标题的话图标会靠左对齐，所以这里用一个空字符串占位）
    NavigationTitle(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
    }
    //导航栏菜单图标（不包含返回箭头）
    NavigationIcons(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.alignSelf(ItemAlign.End);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.create(ThemeGet_1.getTheme(this.theme).IC_PUBLIC_SHARE);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.width(27);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.height(24);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.margin(12);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.visibility(this.addedGoals.length > 0 ? Visibility.Visible : Visibility.Hidden);
            //      Image(getTheme(this.theme).IC_BACK)
            //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
            //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
            //        .width(27)
            //        .height(24)
            //        .margin(12)
            //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
            //        .onClick(() => {
            //          if (this.channel !== null) {
            //            this.channel.callMethod('finishActivity', null);
            //          }
            //        })
            //        .align(Alignment.Start)
            //      Image($r('app.media.share'))
            Image.onClick(() => {
                _ohos_router_1.pushUrl({ url: 'pages/share-preview' });
                this.i18nTest(); //i18n测试代码
            });
            if (!isInitialRender) {
                //      Image(getTheme(this.theme).IC_BACK)
                //        // Defect:设置长宽时需要手动维持 navigation menu icon的原图长宽比
                //        // 否则图片会有截断（两个icon的width和height不统一就是为了防止截断）
                //        .width(27)
                //        .height(24)
                //        .margin(12)
                //        .visibility(this.useBridge ? Visibility.Visible : Visibility.Hidden)
                //        .onClick(() => {
                //          if (this.channel !== null) {
                //            this.channel.callMethod('finishActivity', null);
                //          }
                //        })
                //        .align(Alignment.Start)
                //      Image($r('app.media.share'))
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //      Image($r('app.media.ic_public_history'))
            Image.create(ThemeGet_1.getTheme(this.theme).IC_PUBLIC_HIS);
            //      Image($r('app.media.ic_public_history'))
            Image.width(26);
            //      Image($r('app.media.ic_public_history'))
            Image.height(26);
            //      Image($r('app.media.ic_public_history'))
            Image.margin(12);
            //      Image($r('app.media.ic_public_history'))
            Image.onClick(() => {
                this.intlTest(); //intl测试代码
                _ohos_router_1.pushUrl({ url: 'pages/history_goals' });
                _ohos_promptAction_1.showToast({
                    message: `Today is ${new Date().toDateString()}`,
                    duration: 2500
                });
            });
            //      Image($r('app.media.ic_public_history'))
            Image.id('test_snapshot');
            if (!isInitialRender) {
                //      Image($r('app.media.ic_public_history'))
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
    }
    //需要点亮的目标
    TodoFlagCard(goal, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            __Row__AsCard();
            Row.aspectRatio(1);
            Row.onClick(() => {
                _ohos_router_1.pushUrl({
                    url: 'pages/add-new-target',
                    params: {
                        goal: goal,
                        isAddingTarget: true
                    }
                });
            });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create({ alignContent: Alignment.BottomEnd });
            Stack.width('100%');
            Stack.height('100%');
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(goal.image);
            Image.width('80%');
            Image.aspectRatio(1);
            Image.offset({ x: 12, y: 12 });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.height('100%');
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(goal.goalName);
            Text.fontSize(18);
            Text.fontColor(0x1A1A1A);
            Text.fontWeight(500);
            __Text__generalTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`${helpers_1.numberWithCommas(goal.activeNum)} 人坚持`);
            Text.margin({ top: 2 });
            Text.fontSize(14);
            Text.opacity(0.6);
            __Text__generalTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('点亮目标');
            Button.stateEffect(false);
            Button.backgroundColor(0xFFF2E8);
            Button.fontColor(0xFF841A);
            Button.fontSize(12);
            Button.height(28);
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Column.pop();
        Stack.pop();
        Row.pop();
    }
    //已经添加的目标
    AddedFlagCard(goal, parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            __Row__AsCard();
            Row.height(84);
            Row.onClick(() => {
                _ohos_router_1.pushUrl({
                    url: 'pages/flag-calendar',
                    params: {
                        goalType: goal.goalType,
                        activeNum: this.getActiveNum(goal.goalType)
                    }
                });
            });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.height('100%');
            Column.alignItems(HorizontalAlign.Start);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create({ space: 8 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(goal.icon);
            Image.width(18);
            Image.aspectRatio(1);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(goal.goalName);
            Text.fontSize(14);
            Text.opacity(0.6);
            Text.fontColor(0X1A1A1A);
            Text.fontFamily('HarmonyHeiTi-Medium');
            __Text__generalTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(helpers_1.numberWithCommas(goal.completeValue));
            Text.fontSize(30);
            Text.margin({ right: 3 });
            __Text__generalTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`/${helpers_1.numberWithCommas(goal.goalValue)} ${goal.unit}`);
            Text.fontSize(12);
            Text.opacity(0.6);
            __Text__generalTextStyle();
            Text.offset({ y: 4 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (!this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new ProgressCircle_1.ProgressCircle(this, {
                                    bgStartAngle: goal.isWeight ? Math.PI * 1 / 4 : 0,
                                    bgEndAngle: goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 2,
                                    arcStartAngle: goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2,
                                    arcEndAngle: (goal.isWeight ? (Math.PI * 3 / 2) : (Math.PI * 2))
                                        * goal.progress + (goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2),
                                    startColor: goal.color[0],
                                    endColor: goal.color[1],
                                    isCounterClockWise: goal.isWeight,
                                    //TODO: 硬编码 canvas大小
                                    canvasSize: 84 - 2 * 12,
                                    slot: () => {
                                        this.observeComponentCreation((elmtId, isInitialRender) => {
                                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                            Text.create(`${(goal.progress * 100).toFixed()}%`);
                                            Text.fontSize(12);
                                            Text.opacity(0.6);
                                            __Text__generalTextStyle();
                                            if (!isInitialRender) {
                                                Text.pop();
                                            }
                                            ViewStackProcessor.StopGetAccessRecording();
                                        });
                                        Text.pop();
                                    }
                                }, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Row.pop();
    }
    //主页面
    Page(parent = null) {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.width('100%');
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //TODO: 硬解码标题长度
            //Defect: 斜体中文的最后一个文字会被截断,需要手动延长 Text 组件
            Text.create({ "id": 16777220, "type": 10003, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            __Text__headerText(40, 'bold');
            //TODO: 硬解码标题长度
            //Defect: 斜体中文的最后一个文字会被截断,需要手动延长 Text 组件
            Text.width(170);
            //TODO: 硬解码标题长度
            //Defect: 斜体中文的最后一个文字会被截断,需要手动延长 Text 组件
            Text.margin({ left: 25, top: 15 });
            __Text__generalTextStyle();
            if (!isInitialRender) {
                //TODO: 硬解码标题长度
                //Defect: 斜体中文的最后一个文字会被截断,需要手动延长 Text 组件
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        //TODO: 硬解码标题长度
        //Defect: 斜体中文的最后一个文字会被截断,需要手动延长 Text 组件
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(this.currYear.toString());
            Text.alignSelf(ItemAlign.End);
            Text.backgroundImage({ "id": 16777302, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Text.backgroundImageSize(ImageSize.Contain);
            Text.backgroundImagePosition(Alignment.Center);
            Text.width(50);
            Text.height(16);
            Text.textAlign(TextAlign.Center);
            Text.fontColor(Color.White);
            Text.fontSize(12);
            Text.fontStyle(FontStyle.Italic);
            Text.margin({ bottom: 6 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Plan Your Goals');
            __Text__headerText(22, '500');
            Text.margin({ left: 25, right: 25, bottom: 20 });
            __Text__generalTextStyle();
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.isLoading) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        LoadingProgress.create();
                        LoadingProgress.width('20%');
                        LoadingProgress.offset({ y: '-10%' });
                        if (!isInitialRender) {
                            LoadingProgress.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                });
            }
            else {
                If.branchId(1);
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (!this.isConnectNetwork) {
                this.ifElseBranchUpdateFunction(0, () => {
                    {
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            if (isInitialRender) {
                                ViewPU.create(new NoNetwork_1.NoNetwork(this, {}, undefined, elmtId));
                            }
                            else {
                                this.updateStateVarsOfChildByElmtId(elmtId, {});
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                    }
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Grid.create();
                        Grid.scrollBar(BarState.Off);
                        Grid.columnsTemplate('1fr 1fr');
                        Grid.columnsGap(8);
                        Grid.margin({ left: 12, right: 12 });
                        if (!isInitialRender) {
                            Grid.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ForEach.create();
                        const forEachItemGenFunction = _item => {
                            const item = _item;
                            {
                                const isLazyCreate =  true && (Grid.willUseProxy() === true);
                                const itemCreation = (elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    GridItem.create(deepRenderFunction, isLazyCreate);
                                    GridItem.columnStart(1);
                                    GridItem.columnEnd(2);
                                    if (!isInitialRender) {
                                        GridItem.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                };
                                const observedShallowRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    GridItem.pop();
                                };
                                const observedDeepRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    this.AddedFlagCard.bind(this)(item);
                                    GridItem.pop();
                                };
                                const deepRenderFunction = (elmtId, isInitialRender) => {
                                    itemCreation(elmtId, isInitialRender);
                                    this.updateFuncByElmtId.set(elmtId, itemCreation);
                                    this.AddedFlagCard.bind(this)(item);
                                    GridItem.pop();
                                };
                                if (isLazyCreate) {
                                    observedShallowRender();
                                }
                                else {
                                    observedDeepRender();
                                }
                            }
                        };
                        this.forEachUpdateFunction(elmtId, this.addedGoals, forEachItemGenFunction, (item) => item.goalType.toString(), false, false);
                        if (!isInitialRender) {
                            ForEach.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    ForEach.pop();
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ForEach.create();
                        const forEachItemGenFunction = _item => {
                            const item = _item;
                            {
                                const isLazyCreate =  true && (Grid.willUseProxy() === true);
                                const itemCreation = (elmtId, isInitialRender) => {
                                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                    GridItem.create(deepRenderFunction, isLazyCreate);
                                    if (!isInitialRender) {
                                        GridItem.pop();
                                    }
                                    ViewStackProcessor.StopGetAccessRecording();
                                };
                                const observedShallowRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    GridItem.pop();
                                };
                                const observedDeepRender = () => {
                                    this.observeComponentCreation(itemCreation);
                                    this.TodoFlagCard.bind(this)(item);
                                    GridItem.pop();
                                };
                                const deepRenderFunction = (elmtId, isInitialRender) => {
                                    itemCreation(elmtId, isInitialRender);
                                    this.updateFuncByElmtId.set(elmtId, itemCreation);
                                    this.TodoFlagCard.bind(this)(item);
                                    GridItem.pop();
                                };
                                if (isLazyCreate) {
                                    observedShallowRender();
                                }
                                else {
                                    observedDeepRender();
                                }
                            }
                        };
                        this.forEachUpdateFunction(elmtId, this.todoGoals.filter(todo => {
                            let isActivated = false;
                            this.addedGoals.forEach((added) => {
                                if (todo.goalType === added.goalType) {
                                    isActivated = true;
                                }
                            });
                            return !isActivated;
                        }), forEachItemGenFunction, (item) => item.goalType.toString(), false, false);
                        if (!isInitialRender) {
                            ForEach.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    ForEach.pop();
                    {
                        const isLazyCreate =  true && (Grid.willUseProxy() === true);
                        const itemCreation = (elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            GridItem.create(deepRenderFunction, isLazyCreate);
                            // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                            GridItem.columnStart(1);
                            // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                            GridItem.columnEnd(2);
                            if (!isInitialRender) {
                                // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                                GridItem.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        };
                        const observedShallowRender = () => {
                            this.observeComponentCreation(itemCreation);
                            // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                            GridItem.pop();
                        };
                        const observedDeepRender = () => {
                            this.observeComponentCreation(itemCreation);
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                Column.create();
                                Column.height(150);
                                if (!isInitialRender) {
                                    Column.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                            Column.pop();
                            // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                            GridItem.pop();
                        };
                        const deepRenderFunction = (elmtId, isInitialRender) => {
                            itemCreation(elmtId, isInitialRender);
                            this.updateFuncByElmtId.set(elmtId, itemCreation);
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                Column.create();
                                Column.height(150);
                                if (!isInitialRender) {
                                    Column.pop();
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                            Column.pop();
                            // TODO: 硬解码占位符撑起滚动条，解决滑不到底的问题
                            GridItem.pop();
                        };
                        if (isLazyCreate) {
                            observedShallowRender();
                        }
                        else {
                            observedDeepRender();
                        }
                    }
                    Grid.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        Column.pop();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.width('100%');
            Column.height('100%');
            Column.backgroundColor({ "id": 16777239, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create({ "id": 16777294, "type": 20000, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
            Image.position({ x: 0, y: -5 });
            Image.width('100%');
            Image.objectFit(ImageFit.Contain);
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // TODO: 导航栏抽出
            Navigation.create();
            // TODO: 导航栏抽出
            Navigation.menus({ builder: () => {
                    this.NavigationIcons.call(this);
                } });
            // TODO: 导航栏抽出
            Navigation.size({ width: '100%', height: '100%' });
            // TODO: 导航栏抽出
            Navigation.titleMode(NavigationTitleMode.Mini);
            // TODO: 导航栏抽出
            Navigation.title({ builder: () => {
                    this.NavigationTitle.call(this);
                } });
            // TODO: 导航栏抽出
            Navigation.hideBackButton(false);
            if (!isInitialRender) {
                // TODO: 导航栏抽出
                Navigation.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.Page.bind(this)();
        // TODO: 导航栏抽出
        Navigation.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
function __Text__headerText(fontSize, fontWeight) {
    Text.fontSize(fontSize);
    Text.fontWeight(fontWeight);
    Text.fontStyle(FontStyle.Italic);
    Text.alignSelf(ItemAlign.Start);
}
function __Text__generalTextStyle() {
    Text.fontColor({ "id": 16777240, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
}
function __Row__AsCard() {
    Row.margin({ top: 12, right: 4, left: 4 });
    Row.padding(12);
    Row.borderRadius(16);
    Row.backgroundColor({ "id": 16777238, "type": 10001, params: [], "bundleName": "com.huawei.health", "moduleName": "entry" });
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new Index(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();


/***/ }),

/***/ "../../api/@ohos.app.ability.common.d.ts":
/*!***********************************************!*\
  !*** ../../api/@ohos.app.ability.common.d.ts ***!
  \***********************************************/
/***/ (() => {



/***/ }),

/***/ "../../api/@ohos.hilog.d.ts":
/*!**********************************!*\
  !*** ../../api/@ohos.hilog.d.ts ***!
  \**********************************/
/***/ (() => {



/***/ }),

/***/ "../../api/@ohos.multimedia.image.d.ts":
/*!*********************************************!*\
  !*** ../../api/@ohos.multimedia.image.d.ts ***!
  \*********************************************/
/***/ (() => {



/***/ }),

/***/ "../../api/@ohos.util.d.ts":
/*!*********************************!*\
  !*** ../../api/@ohos.util.d.ts ***!
  \*********************************/
/***/ (() => {



/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		var commonCachedModule = globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] ? globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId]: null;
/******/ 		if (commonCachedModule) { return commonCachedModule.exports; }
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		function isCommonModue(moduleId) {
/******/ 		                if (globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"]) {
/******/ 		                  const length = globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"].length;
/******/ 		                  switch (length) {
/******/ 		                    case 1:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId];
/******/ 		                    case 2:
/******/ 		                      return globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][0][1][moduleId] ||
/******/ 		                      globalThis["webpackChunk_5047ee604ae08900cdea13aa257f5259"][1][1][moduleId];
/******/ 		                  }
/******/ 		                }
/******/ 		                return undefined;
/******/ 		              }
/******/ 		if (globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"] && String(moduleId).indexOf("?name=") < 0 && isCommonModue(moduleId)) {
/******/ 		  globalThis["__common_module_cache___5047ee604ae08900cdea13aa257f5259"][moduleId] = module;
/******/ 		}
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/pages/Index.ets?entry");
/******/ 	_5047ee604ae08900cdea13aa257f5259 = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=Index.js.map