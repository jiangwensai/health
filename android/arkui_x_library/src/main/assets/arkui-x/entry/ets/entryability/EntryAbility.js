var _5047ee604ae08900cdea13aa257f5259;
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/entryability/EntryAbility.ts?entry":
/*!************************************************************************************************************************!*\
  !*** ../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/entryability/EntryAbility.ts?entry ***!
  \************************************************************************************************************************/
/***/ (function(__unused_webpack_module, exports) {


var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var _ohos_net_connection_1  = globalThis.requireNapi('net.connection');
var _ohos_app_ability_UIAbility_1  = globalThis.requireNapi('app.ability.UIAbility');
var _ohos_hilog_1  = globalThis.requireNapi('hilog');
var _ohos_window_1  = globalThis.requireNapi('window');
class EntryAbility extends _ohos_app_ability_UIAbility_1 {
    constructor() {
        super(...arguments);
        this.netCon = _ohos_net_connection_1.createNetConnection();
    }
    onCreate(want, launchParam) {
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
    }
    onDestroy() {
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
    }
    onWindowStageCreate(windowStage) {
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', 'start onWindowStageCreate');
        this.initNetwork();
        this.initWindow();
        // Main window is created, set main page for this ability
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
        windowStage.loadContent('pages/Index', (err, data) => {
            var _a, _b;
            if (err.code) {
                _ohos_hilog_1.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', (_a = JSON.stringify(err)) !== null && _a !== void 0 ? _a : '');
                return;
            }
            _ohos_hilog_1.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', (_b = JSON.stringify(data)) !== null && _b !== void 0 ? _b : '');
        });
    }
    onWindowStageDestroy() {
        // Main window is destroyed, release UI related resources
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', 'start register');
        this.netCon.unregister(function (error) {
            _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', 'connection unregister');
        });
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
    }
    onForeground() {
        // Ability has brought to foreground
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
    }
    onBackground() {
        // Ability has back to background
        _ohos_hilog_1.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
    }
    async initWindow() {
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', 'start initWindow');
        let win = await _ohos_window_1.getLastWindow(this.context);
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow win=${win}`);
        let area = win.getWindowAvoidArea(_ohos_window_1.AvoidAreaType.TYPE_SYSTEM);
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow area=${area}`);
        AppStorage.SetOrCreate('stateBarHeight', area.topRect.height);
        AppStorage.SetOrCreate('navigationBarHeight', area.bottomRect.height);
        //    AppStorage.SetOrCreate('stateBarHeight', 60);
        //    AppStorage.SetOrCreate('navigationBarHeight', 60);
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow area.topRect.height=${area.topRect.height},area.bottomRect.height=${area.bottomRect.height}`);
    }
    async initNetwork() {
        if (await this.isHasNet()) {
            AppStorage.SetOrCreate('isConnectNetwork', true);
        }
        else {
            AppStorage.SetOrCreate('isConnectNetwork', false);
        }
        // 先使用register接口注册订阅事件
        // this.netCon.register(function (error) {
        //   // 监听网络状态变化
        //   this.netCon.on('netAvailable', function (data) {
        //     AppStorage.SetOrCreate('isConnectNetwork', true);
        //   })
        // })
        //    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'start register');
        //    this.netCon.register(function (error) {
        //      hilog.info(0x0000, 'Interface test tag', '%{public}s', `register error ${JSON.stringify(error)}}`);
        //    })
        // 订阅网络可用事件。调用register后，才能接收到此事件通知
        //    this.netCon.on('netAvailable', function (data) {
        //      hilog.info(0x0000, 'Interface test tag', '%{public}s', `on netAvailable ${JSON.stringify(data)}}`);
    }
    async isHasNet() {
        // let netHandle = await connection.getAllNets();
        // console.log('netHandle = ' + JSON.stringify(netHandle));
        // if (netHandle.length > 0) {
        //   return true;
        // }
        let netHandle = true;
        _ohos_hilog_1.info(0x0000, 'Interface test tag', '%{public}s', 'isHasNet');
        _ohos_net_connection_1.hasDefaultNet(function (error, has) {
            console.log(`isHasNet callback error , ${JSON.stringify(error)}`);
            console.log(`isHasNet callback success , ${has}`);
            //      netHandle=has;
        });
        _ohos_net_connection_1.hasDefaultNet().then(function (has) {
            console.log(`isHasNet callback success , ${has}`);
            //      netHandle = has;
        });
        //    console.log('netHandle = ' + JSON.stringify(netHandle));
        //    hilog.info(0x0000, 'Interface test tag', `%{public}s`,`netHandle = ${JSON.stringify(netHandle)} `);
        return netHandle;
    }
}
globalThis.exports.default = EntryAbility;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["../../../../../arkui-x-project/health-new-fork/health/ohos/entry/src/main/ets/entryability/EntryAbility.ts?entry"](0, __webpack_exports__);
/******/ 	_5047ee604ae08900cdea13aa257f5259 = __webpack_exports__;
/******/ 	
/******/ })()
;
//# sourceMappingURL=EntryAbility.js.map