package com.example.health;

import android.app.Application;
import android.util.Log;

import ohos.stage.ability.adapter.StageApplicationDelegate;

/**
 * Example ace application class, which will load ArkUI-X application instance.
 * StageApplication is provided by ArkUI-X
 *
 * @see <a href=
 * "https://gitee.com/arkui-crossplatform/doc/blob/master/contribute/tutorial/how-to-build-Android-app.md">
 * to build android library</a>
 */
public class MyApplication extends Application {
    private static final String LOG_TAG = "HiHelloWorld";

    private static final String RES_NAME = "res";

    @Override
    public void onCreate() {
        Log.e(LOG_TAG, "MyApplication");
        super.onCreate();
        Log.e(LOG_TAG, "MyApplication onCreate");
        StageApplicationDelegate delegate = new StageApplicationDelegate();
        delegate.initApplication(this);
    }
}
