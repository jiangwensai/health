package com.example.health;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.arkui_x_library.R;

import ohos.ace.adapter.ALog;
import ohos.stage.ability.adapter.StageActivity;

/**
 * Example ace activity class, which will load ArkUI-X ability instance.
 * StageActivity is provided by ArkUI-X
 *
 * @see <a h
 * ref=
 * "https://gitee.com/arkui-crossplatform/doc/blob/master/contribute/tutorial/how-to-build-Android-app.md">
 * to build android library</a>
 */
public class EntryEntryActivity extends StageActivity {

    private BridgeIml bridgeIml;
    private String bridgeName = "Bridge";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("HiHelloWorld", "EntryEntryActivity");

        setInstanceName("com.huawei.health:entry:EntryAbility:");
        super.onCreate(savedInstanceState);
        Log.e("HiHelloWorld", getInstanceId() + "");
        bridgeIml = new BridgeIml(this, bridgeName, getInstanceId());
        bridgeIml.setMessageListener(bridgeIml);
        bridgeIml.setMethodResultListener(bridgeIml);
    }

    public void showImage(Bitmap bitmap) {
        setContentView(R.layout.activity_test);
        ALog.i(" onMessage data : ", "start transform bitmap " + bitmap.getWidth() + "," + bitmap.getHeight());
        ImageView imageView = findViewById(R.id.iv_image);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bridgeIml.setMethodResultListener(null);
        bridgeIml.setMessageListener(null);
        bridgeIml.unRegister(bridgeName);
        bridgeIml.release();
        bridgeIml = null;
    }
}
