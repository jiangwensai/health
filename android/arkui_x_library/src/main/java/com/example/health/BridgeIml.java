package com.example.health;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.util.Base64;
import android.view.View;

import java.io.ByteArrayOutputStream;

import ohos.ace.adapter.ALog;
import ohos.ace.adapter.capability.bridge.BridgePlugin;
import ohos.ace.adapter.capability.bridge.IMessageListener;
import ohos.ace.adapter.capability.bridge.IMethodResult;

/**
 * 跨平台Channel实现，用于接收ts发送的消息，提供ts调用的方法，监听调用结果
 */
public class BridgeIml extends BridgePlugin implements IMessageListener, IMethodResult {

    private static final String TAG = "BridgeIml";

    public String name;
    private Context context;

    private Activity activity;

    public BridgeIml(Activity activity, String bridgeName, int instanceId) {
        super(activity, bridgeName, instanceId);
        this.activity = activity;
        this.name = bridgeName;
        setMessageListener(this);
        setMethodResultListener(this);
        this.context = activity;
    }

    public String getName() {
        return name;
    }

    public String finishActivity() {
        if (activity != null && activity instanceof EntryEntryActivity) {
            activity.finish();
            return "Call finish success";
        }
        return "Call finish failed";
    }

    @Override
    //java给js返回
    public Object onMessage(Object o) {
        ALog.i(TAG + " onMessage data : ", o.toString());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            ALog.i(TAG + " onMessage data : ", "start transform bitmap");
            try {
                ALog.i(TAG + " onMessage data : ", "start transform bitmap decode ,o length = " + o.toString().length());
                for (int i = 0; i < o.toString().length() / 1000; i++) {
                    ALog.i(TAG + " onMessage data : ", "start transform bitmap decode ,o length = " + o.toString().substring(i*1000,(i+1)*1000));
                }
                ALog.i(TAG + " onMessage data : ", "start transform bitmap decode ,o length = " + o.toString().substring(o.toString().length() / 1000*1000));
                byte[] bitmapArray = Base64.decode(o.toString(), Base64.DEFAULT);
                ALog.i(TAG + " onMessage data : ", "start transform bitmap decode ,length = " + bitmapArray.length);
                Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
                ALog.i(TAG + " onMessage data : ", "start transform bitmap bitmap is null = " + (bitmap == null));
                if (activity != null && activity instanceof EntryEntryActivity) {
                    ALog.i(TAG + " onMessage data : ", "start transform bitmap showImage");
                    ((EntryEntryActivity) activity).showImage(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return o;
    }

    @Override
    public void onMessageResponse(Object o) {
        ALog.i(TAG + " onMessageResponse data : ", o.toString());
    }

    @Override
    public void onSuccess(Object o) {
        ALog.i(TAG + " onSuccess data : ", o.toString());
    }

    @Override
    public void onError(String s, int i, String s1) {
        ALog.i(TAG, "onError data : " + s + " , " + i + " , " + s1);
    }

    @Override
    public void onMethodCancel(String s) {
        ALog.i(TAG + " onMethodCancel data : ", s);
    }

    public void release() {
        if (context != null) {
            context = null;
        }
        if (activity != null) {
            activity = null;
        }
    }
}
