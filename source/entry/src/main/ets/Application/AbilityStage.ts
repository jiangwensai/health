import hilog from '@ohos.hilog';
import AbilityStage from "@ohos.app.ability.AbilityStage";
// import abilityManager from '@ohos.app.ability.abilityManager';

//验证一下厂商设备信息接口
import deviceInfo from '@ohos.deviceInfo';

export default class MyAbilityStage extends AbilityStage {
    onCreate() {
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'AbilityStage onCreate');
        //这里测试一下厂商信息能否正确显示，通过deviceInfo查询系统版本
        hilog.info(0x0000, 'testTag1', 'Succeeded in loading the content. Data: %{public}s', deviceInfo.osFullName ?? '');
        //在板子上面模拟深夜模式
        // abilityManager.updateConfiguration({ colorMode:1 }, () => {
        //     hilog.info(0x0000, 'testTag10', 'Succeeded in loading the content. Data: %{public}s',  '');
        // })

    }
    //这个函数可以监听到应用级变化，发生全局配置变更时触发回调，当前全局配置包括系统语言、深浅色模式。
    onConfigurationUpdate(config) {
        hilog.info(0x0000, 'testTag2', 'Succeeded in loading the content. Data: %{public}s', JSON.stringify(config) ?? '');
        let color = config.colorMode;
        AppStorage.SetAndLink("theme", color);

    }
}