import CloudApis from '../common/cloudApis';
import { AddedGoal, TodoGoal } from '../model/Goals';
import { numberWithCommas } from '../common/helpers';
import { ProgressCircle } from '../components/ProgressCircle';
import { getTheme } from '../common/ThemeGet';

@Entry
@Component
struct HistoryGoals {
  @State isLoading: boolean = true;
  @State addedGoals: AddedGoal[] = [];
  @State isEdit: boolean = false;
  @State checkList: AddedGoal[] = [];
  @StorageLink('theme') theme: number = -1; // 初始主题theme值

  build() {
    Navigation() {
      Column() {
        if (this.isLoading) {
          LoadingProgress().width('20%')
        } else {
          if (this.addedGoals.length > 0) {
            List({ space: 12 }) {
              ListItem() {
                this.ThisYearTarget()
              }

              ForEach(this.addedGoals, (item: AddedGoal, index: number) => {
                ListItem() {
                  this.GoalItem(item, index)
                }
              }, (item: AddedGoal) => item.id.toString())

              ListItem() {
                this.LastYearTarget()
              }
            }
            .scrollBar(BarState.Off)
            .layoutWeight(1)
            .margin({ left: 12, right: 12 })
          } else {
            Blank()
            Image($r('app.media.img_nohistory'))
              .size({ width: 150, height: 150 })
              .objectFit(ImageFit.Contain)
            Text('没有历史记录')
              .fontSize(16)
              .fontWeight(FontWeight.Medium)
              .margin({ top: 8, bottom: 150 })
            Blank()
          }
        }

        if (this.isEdit) {
          this.ToolBar()
        }
      }
      .height('100%')
    }
    .menus(this.NavigationIcons())
    .size({ width: '100%', height: '100%' })
    .titleMode(NavigationTitleMode.Mini)
    .hideBackButton(false)
    .title('历史目标')
    //    .backgroundColor('#F9F4EF')
    .backgroundColor(getTheme(this.theme).BACKGROUND_MAIN)
  }

  @Builder NavigationIcons() {
    Row() {
      Image($r('app.media.ic_public_edit'))
        .width(26)
        .height(26)
        .margin(12)
        .onClick(() => {
          this.isEdit = !this.isEdit
        })
    }
  }

  @Builder ToolBar() {
    Column() {
      Image($r('app.media.ic_public_delete'))
        .width(24)
        .height(24)
      Text($r('app.string.delete'))
        .fontSize(10)
        .fontWeight(FontWeight.Medium)
        .margin({ top: 4 })
    }
    .padding({ top: 12, bottom: 12 })
    .onClick(() => {
      for (let i = 0; i < this.checkList.length; i++) {
        let curIndex = this.addedGoals.indexOf(this.checkList[i]);
        this.addedGoals.splice(curIndex, 1);
      }
      this.checkList = [];
      this.isEdit = false;
    })
  }

  @Builder ThisYearTarget() {
    Row() {
      Column() {
        Text(`${new Date().getFullYear()}年，我的目标`)
          .fontSize(18)
          .fontWeight(FontWeight.Medium)
        Text(`达成目标：${this.getReachedTargetNum()}/${this.addedGoals.length}年，我的目标`)
          .fontSize(12)
          .fontWeight(FontWeight.Medium)
      }
      .alignItems(HorizontalAlign.Start)

      Blank()
      Image($r('app.media.ic_completeFlag'))
        .width(100)
        .height(100)
        .objectFit(ImageFit.Contain)
    }
    .padding(20)
    .height(144)
    .width('100%')
    .justifyContent(FlexAlign.Center)
    .backgroundImage($r('app.media.toubu'))
    .backgroundImageSize(ImageSize.Cover)
    .borderRadius(16)
  }

  @Builder LastYearTarget() {
    Row() {
      Column() {
        Text(`${new Date().getFullYear() - 1}年，我的目标`)
          .fontSize(18)
          .fontWeight(FontWeight.Medium)
        Text(`达成目标：${this.getReachedTargetNum()}/${this.addedGoals.length}`)
          .fontSize(12)
          .fontWeight(FontWeight.Medium)
      }
      .alignItems(HorizontalAlign.Start)

      Blank()
      Image($r('app.media.img_youyong'))
        .width(100)
        .height(100)
        .objectFit(ImageFit.Contain)
    }
    .padding(20)
    .height(144)
    .width('100%')
    .justifyContent(FlexAlign.Center)
    .backgroundImage($r('app.media.toubu'))
    .backgroundImageSize(ImageSize.Cover)
    .borderRadius(16)
  }

  @Builder GoalItem(goal: AddedGoal, index: number) {
    Stack() {
      Row() {
        Column() {
          Row({ space: 8 }) {
            Image(goal.icon).width(18).aspectRatio(1)
            Text(goal.goalName)
              .fontSize(14)
              .opacity(0.6)
              .fontColor(0X1A1A1A)
              .fontFamily('HarmonyHeiTi-Medium')
              .generalTextStyle()
          }

          Blank()
          Row() {
            Text(numberWithCommas(goal.completeValue))
              .fontSize(30)
              .margin({ right: 3 })
              .generalTextStyle()
            Text(`/${numberWithCommas(goal.goalValue)} ${goal.unit}`)
              .fontSize(12)
              .opacity(0.6)
              .generalTextStyle()
              .offset({ y: 4 })
          }
        }
        .height('100%')
        .alignItems(HorizontalAlign.Start)

        Blank()
        if (!this.isLoading) {
          ProgressCircle({
            bgStartAngle: goal.isWeight ? Math.PI * 1 / 4 : 0,
            bgEndAngle: goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 2,
            arcStartAngle: goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2,
            arcEndAngle: (goal.isWeight ? Math.PI * 3 / 2 : Math.PI * 2)
            * goal.progress + (goal.isWeight ? Math.PI * 3 / 4 : Math.PI * 3 / 2),
            startColor: goal.color[0],
            endColor: goal.color[1],
            isCounterClockWise: goal.isWeight,
            canvasSize: 84 - 2 * 12,
          }) {
            Text(`${(goal.progress * 100).toFixed()}%`)
              .opacity(0.6)
              .fontSize(12)
              .generalTextStyle()
          }
        }
      }
      .width('100%')
      .height('100%')

      if (this.isEdit) {
        Image(this.checkList.indexOf(goal) === -1 ? $r('app.media.ic_checkbox_unselected') : $r('app.media.ic_checkbox'))
          .size({ width: 24, height: 24 })
          .objectFit(ImageFit.Contain)
      }
    }
    .alignContent(Alignment.TopEnd)
    .width('100%')
    .AsCard()
    .height(84)
    .onClick(() => {
      if (this.isEdit) {
        let curIndex = this.checkList.indexOf(goal);
        if (curIndex === -1) {
          this.checkList.push(goal)
        } else {
          this.checkList.splice(curIndex, 1);
        }
      }
    })
  }

  aboutToAppear() {
    this.initGoal();
  }

  getReachedTargetNum(): number {
    let reachedNum = 0;
    this.addedGoals.forEach((addedGoal) => {
      if (addedGoal.progress >= 1) {
        reachedNum++;
      }
    })
    return reachedNum;
  }

  async initGoal() {
    this.addedGoals = await CloudApis.getGoalList();
    this.isLoading = false;
  }
}

@Extend(Text) function generalTextStyle() {
  .fontColor($r('app.color.font_color_main'))
}

@Extend(Stack) function AsCard() {
  .padding(12)
  .borderRadius(16)
  .backgroundColor($r('app.color.background_card'))
}