/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Create: 2022/10/14
  my_annual_flag: 添加新目标页
*/
import router from '@ohos.router';
import { TodoGoal, AddedGoal } from '../model/Goals';
import { getTimeRange, numberWithCommas } from '../common/helpers';
import { SaveTarget } from '../components/SaveTarget';
import common from '@ohos.app.ability.common';
import { getTheme } from '../common/ThemeGet';

@Entry
@Component
struct AddNewTarget {
  @State goal: TodoGoal = router.getParams()['goal'];
  @State target: string = '';
  @StorageLink('theme') theme: number = -1; // 初始主题theme值

  //导航栏标题（标题不接受Resource类型的字符串，得单独做一个builder）
  @Builder NavigationTitle() {
    Row() {
      Text('年度').fontSize(20).fontWeight(FontWeight.Medium)
      Text(this.goal.goalName).fontSize(20).fontWeight(FontWeight.Medium)
      Text('目标').fontSize(20).fontWeight(FontWeight.Medium)
    }
    .width('100%')
  }

  @Builder Page() {
    RelativeContainer() {
      Column() {
        Text(getTimeRange())
          .margin({ left: 24, right: 24, bottom: 4 })
          .opacity(0.6)
          .fontSize(12)
        Text(this.goal.slogan)
          .fontSize(14)
          .margin({ left: 24, right: 24 })
        Image(this.goal.addImage)
          .width('100%')
          .alignSelf(ItemAlign.Auto)
          .backgroundImageSize(ImageSize.Auto)
          .aspectRatio(1)
          .margin({ left: 24, right: 24 })
      }
      .alignItems(HorizontalAlign.Start)
      .alignRules({
        top: { anchor: "__container__", align: VerticalAlign.Top },
      })
      .id('image-container')

      Column() {
        SaveTarget({ showHistory: false, goal: this.goal })
      }
      //      .backgroundColor(0xffffff)
      .backgroundColor(getTheme(this.theme).START_WINDOW_BACKGROUND)
      .border({ radius: { topLeft: 32, topRight: 32 } })
      .padding({ left: 24, right: 24, bottom: 24, top: 30 })
      .width('100%')
      .alignRules({
        bottom: { anchor: "__container__", align: VerticalAlign.Bottom },
      })
      .focusable(true)
      .id("save-target-card")
    }
    .width('100%')
    .height('100%')
  }

  build() {
    Navigation() {
      this.Page();
    }
    .hideBackButton(false)
    .titleMode(NavigationTitleMode.Mini)
    .title(this.NavigationTitle())
    .size({ width: '100%', height: '100%' })
    //    .backgroundColor(0xf1f3f5)
    .backgroundColor(getTheme(this.theme).BACKGROUND_MAIN)
  }
}