import connection from '@ohos.net.connection';
import UIAbility from '@ohos.app.ability.UIAbility';
import hilog from '@ohos.hilog';
import window from '@ohos.window';

export default class EntryAbility extends UIAbility {
  netCon = connection.createNetConnection();

  onCreate(want, launchParam) {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
  }

  onDestroy() {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'start onWindowStageCreate');
    this.initNetwork()
    this.initWindow()

    // Main window is created, set main page for this ability
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
    windowStage.loadContent('pages/Index', (err, data) => {
      if (err.code) {
        hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '');
        return;
      }
      hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', JSON.stringify(data) ?? '');
    });
  }

  onWindowStageDestroy() {
    // Main window is destroyed, release UI related resources
    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'start register');
    this.netCon.unregister(function (error) {
      hilog.info(0x0000, 'Interface test tag', '%{public}s', 'connection unregister');
    })
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
  }

  onForeground() {
    // Ability has brought to foreground
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
  }

  onBackground() {
    // Ability has back to background
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
  }

  async initWindow(){
    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'start initWindow');
    let win = await window.getLastWindow(this.context);
    hilog.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow win=${win}`);
    let area = win.getWindowAvoidArea(window.AvoidAreaType.TYPE_SYSTEM);
    hilog.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow area=${area}`);
    AppStorage.SetOrCreate('stateBarHeight', area.topRect.height);
    AppStorage.SetOrCreate('navigationBarHeight', area.bottomRect.height);
//    AppStorage.SetOrCreate('stateBarHeight', 60);
//    AppStorage.SetOrCreate('navigationBarHeight', 60);
    hilog.info(0x0000, 'Interface test tag', '%{public}s', `start initWindow area.topRect.height=${area.topRect.height},area.bottomRect.height=${area.bottomRect.height}`);
  }

  async initNetwork(){
    if (await this.isHasNet()) {
      AppStorage.SetOrCreate('isConnectNetwork', true);
    } else {
      AppStorage.SetOrCreate('isConnectNetwork', false);
    }
    // 先使用register接口注册订阅事件
    // this.netCon.register(function (error) {
    //   // 监听网络状态变化
    //   this.netCon.on('netAvailable', function (data) {
    //     AppStorage.SetOrCreate('isConnectNetwork', true);
    //   })
    // })
//    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'start register');
//    this.netCon.register(function (error) {
//      hilog.info(0x0000, 'Interface test tag', '%{public}s', `register error ${JSON.stringify(error)}}`);
//    })
    // 订阅网络可用事件。调用register后，才能接收到此事件通知
//    this.netCon.on('netAvailable', function (data) {
//      hilog.info(0x0000, 'Interface test tag', '%{public}s', `on netAvailable ${JSON.stringify(data)}}`);
  }

  async isHasNet() {
    // let netHandle = await connection.getAllNets();
    // console.log('netHandle = ' + JSON.stringify(netHandle));
    // if (netHandle.length > 0) {
    //   return true;
    // }
    let netHandle=true;
    hilog.info(0x0000, 'Interface test tag', '%{public}s', 'isHasNet');
    connection.hasDefaultNet(function(error,has){
      console.log(`isHasNet callback error , ${JSON.stringify(error)}`);
      console.log(`isHasNet callback success , ${has}`);
//      netHandle=has;
    })
    connection.hasDefaultNet().then(function(has){
      console.log(`isHasNet callback success , ${has}`)
//      netHandle = has;
    })
//    console.log('netHandle = ' + JSON.stringify(netHandle));
//    hilog.info(0x0000, 'Interface test tag', `%{public}s`,`netHandle = ${JSON.stringify(netHandle)} `);
    return netHandle;
  }
}
