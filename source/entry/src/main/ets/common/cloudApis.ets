/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/9/24
  my_annual_flag: 云侧接口调用
 */

import http from '@ohos.net.http';
import { mockCloudData } from './mockData';
import { TodoGoal, AddedGoal, GoalType } from '../model/Goals';

const isMock = true;
const mockDelay = 1000;
const achievementDomain = 'https://lfhealthtest2.hwcloudtest.cn:18444/achievement';
//const huid = '4130086000005055643';

// 13019409302
const huid = '10086000011925744';

const commonBody = {
  appId: 'com.huawei.health',
  appType: 1,
  deviceId: '4fe5240a56f37a11',
  deviceType: 9,
  // token: '0413008600000505564356624a402cc253ef201f90c7230bd25b5060d2e466b1e85fa939d999cd3e7152',

  // 13019409302
  token: '00010086000011925744fdf226b8bfbfefa73011ecca54a0819cd50d0d18ab008273c6e8aa7c86342cda',
  tokenType: 1,
  ts: new Date().getTime(),
}

class CloudApis {
  domain: string
  options: http.HttpRequestOptions
  //  options: {extraData: {}}
  constructor() {
    this.domain = achievementDomain;
    this.options = {
      method: http.RequestMethod.POST,
      extraData: commonBody,
      header: {
        'Content-Type': 'application/json',
        'x-huid': huid
      },
      readTimeout: 10000,
      connectTimeout: 10000
    }
  }

  // 获取 6 种目标的统计数据（如现激活人数等）
  async getGoalStatistics(): Promise<TodoGoal[]> {
    let resResultObj;
    if (isMock) {
      //resResultObj = await new Promise(resolve => setTimeout(() => {
      //  resolve(mockCloudData.getGoalStatistics.result);
      //}, mockDelay))
      resResultObj = mockCloudData.getGoalStatistics.result
    } else {
      const req = http.createHttp();
      const resStr = await req.request(`${this.domain}/getGoalStatistics`, this.options);
      resResultObj = JSON.parse(resStr.result as string);
      console.log(JSON.stringify(resResultObj));
    }
    const todoGoals = resResultObj.statistics.map((e) => {
      return new TodoGoal(e.goalType, e.activeNum);
    })
    return todoGoals;
  }

  // 获取本用户激活的目标的各种数据（如已完成数值、目标数值等）
  async  getGoalList(): Promise<AddedGoal[]> {
    let resResultObj;
    if (isMock) {
      //resResultObj = await new Promise(resolve => setTimeout(() => {
      //  resolve(mockCloudData.getGoalList.result);
      //}, mockDelay));
      resResultObj = mockCloudData.getGoalList.result
    } else {
      const req = http.createHttp();
      const resStr = await req.request(`${this.domain}/getGoalList}`, this.options);
      resResultObj = JSON.parse(resStr.result as string);
      console.log(JSON.stringify(resResultObj));
    }
    const addedGoals: Array<AddedGoal> = resResultObj.goal.map((e) => {
      return new AddedGoal(e.id, e.goalType, e.completeValue, e.goalValue, e.currentWeight, e.recordDays);
    });
    return addedGoals;
  }

  // 添加新目标（除了体重目标）
  async addGoal(goalType: number, goalValue: number) {
    let resResultObj;
    if (isMock) {
      resResultObj = mockCloudData.addGoal.result
    } else {
      const req = http.createHttp();
      const body = {
        timesType: 0,
        goalType: goalType,
        goalValue: goalValue,
        frequency: 0,
        unitValue: 0,
        ...commonBody
      }
      this.options.extraData = body;
      const resStr = await req.request(`${this.domain}/addGoal}`, this.options)
      resResultObj = JSON.parse(resStr.result as string);
    }
    console.log(`CloudApis: addGoal: ${JSON.stringify(resResultObj)}`);
    return resResultObj;
  }

  // 添加体重目标
  async addWeightGoal(initWeight: number, targetWeight: number) {
    let resResultObj;
    if (isMock) {
      resResultObj = mockCloudData.addGoal.result
    } else {
      const req = http.createHttp();
      const body = {
        timesType: 0,
        goalType: GoalType.Weight,
        goalValue: targetWeight,
        frequency: 0,
        unitValue: 0,
        ...commonBody
      }
      this.options.extraData = body;
      const resStr = await req.request(`${this.domain}/addGoal}`, this.options)
      resResultObj = JSON.parse(resStr.result as string);
    }
    console.log(`CloudApis: addWightGoal: ${JSON.stringify(resResultObj)}`);
    return resResultObj;
  }

  // 删除目标
  async delGoal(addedGoal: AddedGoal) {
    let resResultObj;
    if (isMock) {
      resResultObj = mockCloudData.delGoal.result
    } else {
      const req = http.createHttp();
      const body = {
        records: [addedGoal.id],
        ...commonBody
      }
      this.options.extraData = body;
      const resStr = await req.request(`${this.domain}/delGoal}`, this.options)
      resResultObj = JSON.parse(resStr.result as string);
    }
    console.log(`CloudApis: delGoal: ${JSON.stringify(resResultObj)}`);
    return resResultObj;
  }

  // 修改编辑目标
  async updateGoal(addedGoal: AddedGoal, goalValue: number, currentWeight = 0) {
    let resResultObj;
    if (isMock) {
      resResultObj = mockCloudData.updateGoal.result
    } else {
      const req = http.createHttp();
      const body = {
        timesType:0,
        id: addedGoal.id,
        goalValue: goalValue,
        frequency: 0,
        unitValue: 0,
        currentWeight: 0,
        ...commonBody
      }
      if (addedGoal.goalType === GoalType.Weight) {
        body.currentWeight = currentWeight;
      }
      this.options.extraData = body;
      const resStr = await req.request(`${this.domain}/updateGoal}`, this.options)
      resResultObj = JSON.parse(resStr.result as string);
    }
    console.log(`CloudApis: updateGoal: ${JSON.stringify(resResultObj)}`);
    return resResultObj;
  }
}

export default new CloudApis();
