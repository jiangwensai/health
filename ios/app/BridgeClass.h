//
//  BridgeClass.h
//  app
//
//  Created by Winsom on 2023/5/17.
//

#import <libarkui_ios/BridgePlugin.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BridgeClass : BridgePlugin<IMethodResult, IMessageListener>

@property (copy, nonatomic) void (^block)(NSString *method);

- (void)finishActivity;

- (void)addFinishBlock:(void(^)(NSString *method))block;

@end

NS_ASSUME_NONNULL_END
