//
//  BridgeClass.m
//  app
//
//  Created by Winsom on 2023/5/17.
//

#import "BridgeClass.h"

@implementation BridgeClass

- (void)addFinishBlock:(void (^)(NSString *method))block {
    _block = block;
}

- (void)finishActivity {
    if (_block) {
        _block(@"finishActivity");
    }
}

#pragma mark - listener
- (id)onMessage:(id)data {
    
    return @{@"response: first":@"response: second"};
}

- (void)onMessageResponse:(id)data {
    
}

- (void)onSuccess:(NSString *)methodName
      resultValue:(id)resultValue {
    
}

- (void)onError:(NSString *)methodName
      errorCode:(ErrorCode)errorCode
   errorMessage:(NSString *)errorMessage {

    NSLog(@"%s methodName %@ ,errorCode %d, errorMessage %@",__func__,methodName,errorCode,errorMessage);
}

- (void)onMethodCancel:(NSString *)methodName {
    NSLog(@"%s methodName %@ ",__func__,methodName);
}

@end
