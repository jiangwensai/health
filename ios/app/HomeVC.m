//
//  HomeVC.m
//  app
//
//  Created by Winsom on 2023/5/16.
//

#import "HomeVC.h"
#import "EntryMainViewController.h"

#define BUNDLE_DIRECTORY @"arkui-x"
#define BUNDLE_NAME @"com.example.abc"

@interface HomeVC ()

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *button = UIButton.new;
    button.frame = CGRectMake(10, 100, 100, 100);
    [button setTitle:@"年度目标" forState:UIControlStateNormal];
    [button setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(jump:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)jump:(id)sender {
    
    NSString *instanceName = [NSString stringWithFormat:@"%@:%@:%@",BUNDLE_NAME, @"entry", @"EntryAbility"];
    EntryMainViewController *mainView = [[EntryMainViewController alloc] initWithInstanceName:instanceName];
    
//    [self.navigationController pushViewController:mainView animated:YES];
    [self.navigationController presentViewController:mainView animated:YES completion:^{

    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
