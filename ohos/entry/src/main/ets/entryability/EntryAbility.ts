import connection from '@ohos.net.connection';
import UIAbility from '@ohos.app.ability.UIAbility';
import hilog from '@ohos.hilog';
import window from '@ohos.window';

export default class EntryAbility extends UIAbility {
  // netCon = connection.createNetConnection();

  onCreate(want, launchParam) {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
  }

  onDestroy() {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    this.initNetwork()
    this.initWindow()

    // Main window is created, set main page for this ability
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
    windowStage.loadContent('pages/Index', (err, data) => {
      if (err.code) {
        hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '');
        return;
      }
      hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', JSON.stringify(data) ?? '');
    });
  }

  onWindowStageDestroy() {
    // Main window is destroyed, release UI related resources
    // this.netCon.unregister(function (error) {
    //   hilog.info(0x0000, 'testTag', '%{public}s', 'connection unregister');
    // })
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
  }

  onForeground() {
    // Ability has brought to foreground
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
  }

  onBackground() {
    // Ability has back to background
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
  }

  async initWindow(){
    let win = await window.getLastWindow(this.context);
    let area = win.getWindowAvoidArea(window.AvoidAreaType.TYPE_SYSTEM);
    AppStorage.SetOrCreate('stateBarHeight', area.topRect.height);
    AppStorage.SetOrCreate('navigationBarHeight', area.bottomRect.height);
  }

  async initNetwork(){
    if (await this.isHasNet()) {
      AppStorage.SetOrCreate('isConnectNetwork', true);
    } else {
      AppStorage.SetOrCreate('isConnectNetwork', false);
    }
    // 先使用register接口注册订阅事件
    // this.netCon.register(function (error) {
    //   // 监听网络状态变化
    //   this.netCon.on('netAvailable', function (data) {
    //     AppStorage.SetOrCreate('isConnectNetwork', true);
    //   })
    // })
  }

  async isHasNet() {
    // let netHandle = await connection.getAllNets();
    // console.log('netHandle = ' + JSON.stringify(netHandle));
    // if (netHandle.length > 0) {
    //   return true;
    // }
    return true;
  }
}
