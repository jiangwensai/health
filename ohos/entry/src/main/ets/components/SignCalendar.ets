/*
  Copyright (c) Huawei Technologies Co., Ltd. 2022. All rights reserved.
  Created: 2022/10/24
  my_annual_flag: 签到日历组件，用于我的目标详情页下方上拉 panel 中。
 */
import { signedDates } from '../common/mockData';
import { AddedGoal } from '../model/Goals';
@Component
export struct SignCalendar {
  private currentYear = new Date().getFullYear();

  // 签到的日期集合
  private recordDaysTimeStamps: Array<number> = [];
  private recordDays: Set<string>;

  aboutToAppear() {
    this.recordDays = new Set<string>();
    this.recordDaysTimeStamps.forEach((timestamp) => {
      this.recordDays.add((new Date(timestamp)).toDateString());
    });
  }

  // 日历中的圆点
  @Builder CalendarCircle(signed: boolean) {
    Circle({ width: 10, height: 10 })
      .fill(signed ? 0xff841a : 0xd8d8d8)
      .opacity(0.6)
      .margin({ top: 4, bottom: 4})
  }

  /**
   * 获得今年某一月第一天是星期几
   * @param monthIndex 月份索引（0 到 11）
   * @return 返回星期几（0 到 6，分别是星期日、星期一 ...）
   */
  getFirstWeekDayOfMonth(monthIndex: number): number {
    return new Date(this.currentYear, monthIndex, 1).getDay();
  }

  /**
   * 获得今年某一月天数
   * @param monthIndex 月份索引（0 到 11）
   * @return 该月天数
   */
  getNumOfDays(monthIndex): number {
    return new Date(this.currentYear, monthIndex + 1, 0).getDate();
  };


  build() {
    Column() {
      Row() {
        ForEach(Array(2).fill(0),
          () => {
            Row() {
              Column() {}
              .width('17%')
              Column() {
                Grid() {
                  GridItem(){ Text('日') }
                  GridItem(){ Text('一') }
                  GridItem(){ Text('二') }
                  GridItem(){ Text('三') }
                  GridItem(){ Text('四') }
                  GridItem(){ Text('五') }
                  GridItem(){ Text('六') }
                }
                .columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr')
              }
              .width('83%')
            }
            .width('50%')
          },
        )
      }.height(25)
      Scroll() {
        Flex({
          direction: FlexDirection.Row,
          wrap: FlexWrap.Wrap,
        }) {
          ForEach(Array(12).fill(0),
            (_, m) => {
              Row() {
                Column() {
                  Text(`${12 - m}月`).margin({ top: 2, right: -2 })
                  Row().width(18).height(2).borderRadius(4).margin({ top: 2 })
                    .linearGradient({
                      direction: GradientDirection.Right,
                      colors: [['rgba(251, 101, 34, 0.5)', 0.0], ['rgba(255, 149, 71, 0.3)', 1.0]]
                    })
                }
                .alignItems(HorizontalAlign.End)
                .width('17%')
                Column() {
                  Grid() {
                    ForEach(Array(this.getFirstWeekDayOfMonth(11 - m)).fill(0),
                      () => { GridItem() {} },
                      (_, i) => `vacuum_${12 - m}-${i}`
                    )
                    ForEach(Array(this.getNumOfDays(11 - m)).fill(0),
                      (_, d) => { GridItem() {
                        this.CalendarCircle(this.recordDays.has((new Date(this.currentYear, 11 - m, d + 1)).toDateString()))
                      }},
                      (_, d) => `date_${12 - m}-${d + 1}`
                    )
                  }
                  // 使 Grid 不会阻挡滑动
                  .hitTestBehavior(HitTestMode.None)
                  .columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr')
                }
                .width('83%')
              }
              .alignItems(VerticalAlign.Top)
              .width('50%')
              .height(120)
            },
            (_, m) => `month_${12 - m}`
          )
          // TODO: 硬编码一行日历的高度撑起滚动条，解决滑不到底的问题
          Column().height(300)
        }
      }
      .height(300)
      .scrollBar(BarState.Off)
    }
  }
}